# Chat 8

This demo shows how to add bldg25-chat to an existing angular 6+ application

See Setup Demo App below

[![Build Status][travis-image]][travis-url] [![Coverage Status][coveralls-image]][coveralls-url]
[![Known Vulnerabilities](https://snyk.io/test/github/nickmerwin/node-coveralls/badge.svg)](https://snyk.io/test/github/nickmerwin/node-coveralls)

[travis-image]: https://travis-ci.org/nickmerwin/node-coveralls.svg?branch=master
[travis-url]: https://travis-ci.org/nickmerwin/node-coveralls
[coveralls-image]: https://coveralls.io/repos/nickmerwin/node-coveralls/badge.svg?branch=master&service=github
[coveralls-url]: https://coveralls.io/github/nickmerwin/node-coveralls?branch=master

## Setup

- clone repo

```bash
git clone https://github.com/MemoryChips/bldg25-chat-demo.git
```

1. Clone repo
2. mkdir keys

- Create jwt keys

```bash
cd keys
ssh-keygen -t rsa -b 1024 -f jwt-key # with no passphrase
# use this format of the public key in the server
ssh-keygen -f jwt-key.pub -e -m pem > jwt-key.pem.pub # creates pem format
```

- Create secure server key and self-signed certificate

```bash
# create openssl-custom.cnf
[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn

[dn]
C = US
ST = NC
L = Apex
O = Web Development
OU = IT Department
emailAddress = memorychips@yahoo.com
CN = localhost

[v3_req]
subjectAltName = @alt_names

[alt_names]
DNS.1 = *.localhost
DNS.2 = localhost
# end cnf file

cd keys
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout server.key \
    -new \
    -out server.crt \
    -config ./openssl-custom.cnf \
    -sha256 \
    -days 365
```

3. Copy .env-example to .env

- Replace environment vars as per instructions

4. add image files as desired to ./src/assets/images

```bash
# cd to project dir
# set the next line to the source of your image files
SOURCE_OF_IMAGEFILES=../../bldg25-chat-demo/src/assets/images/*
mkdir ./src/assets/images
cp $SOURCE_OF_IMAGEFILES ./src/assets/images
```

5. choose and run a launch scenario
