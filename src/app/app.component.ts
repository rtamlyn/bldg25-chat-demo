import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'

// Must be added to application
import { ChatLoginService } from 'bldg25-chat'
import { AuthService } from './auth/auth.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Chat 6 Demo'

  constructor(
    private chatLoginService: ChatLoginService,
    private authService: AuthService,
    private router: Router
  ) {
    this.authService.me$.subscribe(_user => {
      const returnUrl = localStorage.getItem('returnUrl')
      if (!returnUrl) return
      localStorage.removeItem('returnUrl')
      setTimeout(() => {
        this.router.navigateByUrl(returnUrl)
      }, 0)
    })
    this.authService.me$.subscribe(me => {
      this.chatLoginService.setLoggedInState({
        loggedIn: me.email !== '' && me.email !== 'guest',
        user: me,
        token: 'here is a fake token'
      })
    })
  }

  ngOnInit() {}
}
