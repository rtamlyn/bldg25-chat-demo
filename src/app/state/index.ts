import { ActionReducerMap, createSelector, MetaReducer } from '@ngrx/store'
import { environment } from '../../environments/environment'
import * as fromProduct from './product/product.reducer'

export interface State {
  // auth: fromAuth.State
  product: fromProduct.State
}

export const reducers: ActionReducerMap<State> = {
  // auth: fromAuth.reducer,
  product: fromProduct.reducer
}

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : []

export const selectProducts = createSelector(
  (state: State) => state.product,
  (state: fromProduct.State) => state.entities
)

export const selectAllProducts = createSelector(
  (state: State) => state.product,
  fromProduct.selectAll
)

export const selectProductsLoaded = createSelector(
  (state: State) => state.product,
  (state: fromProduct.State) => state.loadedFromServer
)

// export const selectIsLoggedIn = createSelector(
//   (state: State) => state.auth,
//   (state: fromAuth.State) => state.me._id !== ''
// )

// export const selectLoginFailReason = createSelector(
//   (state: State) => state.auth,
//   (state: fromAuth.State) => state.loginFailReason
// )

// export const selectSignupFailReason = createSelector(
//   (state: State) => state.auth,
//   (state: fromAuth.State) => state.signupFailReason
// )

// export const selectMe = createSelector(
//   (state: State) => state.auth,
//   (state: fromAuth.State) => state.me
// )

// export const selectReturnUrl = createSelector(
//   (state: State) => state.auth,
//   (state: fromAuth.State) => state.returnUrl
// )
