import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity'
// import { Product } from './product.model'
import { Product } from '../../shared/services/product.model'
import { ProductActions, ProductActionTypes } from './product.actions'
import { Action } from '@ngrx/store'

export interface State extends EntityState<Product> {
  // additional entities state properties
  loadedFromServer: boolean
}

export const adapter: EntityAdapter<Product> = createEntityAdapter<Product>()

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
  loadedFromServer: false
})

export function reducer(state = initialState, theAction: Action): State {
  const action = theAction as ProductActions
  switch (action.type) {
    case ProductActionTypes.AddProduct: {
      return adapter.addOne(action.payload.product, state)
    }

    case ProductActionTypes.UpsertProduct: {
      return adapter.upsertOne(action.payload.product, state)
    }

    case ProductActionTypes.AddProducts: {
      return adapter.addMany(action.payload.products, state)
    }

    case ProductActionTypes.UpsertProducts: {
      return adapter.upsertMany(action.payload.products, state)
    }

    case ProductActionTypes.UpdateProduct: {
      return adapter.updateOne(action.payload.product, state)
    }

    case ProductActionTypes.UpdateProducts: {
      return adapter.updateMany(action.payload.products, state)
    }

    case ProductActionTypes.DeleteProduct: {
      return adapter.removeOne(action.payload.id, state)
    }

    case ProductActionTypes.DeleteProducts: {
      return adapter.removeMany(action.payload.ids, state)
    }

    case ProductActionTypes.LoadProducts: {
      return adapter.addAll(action.payload.products, { ...state, loadedFromServer: true })
    }

    case ProductActionTypes.ClearProducts: {
      return adapter.removeAll(state)
    }

    default: {
      console.log(`Warning: product default reducer case called: ${theAction.type}.`)
      return state
    }
  }
}

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors()
