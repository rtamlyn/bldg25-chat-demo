import { TestBed } from '@angular/core/testing'

import { StateService } from './state.service'
import { Store } from '@ngrx/store'
import { of } from 'rxjs'

class MockStore {
  select(_selection: string) {
    return of(null)
  }
}

describe('StateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}))
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: Store, useClass: MockStore }]
    })
  })

  it('should be created', () => {
    const service: StateService = TestBed.get(StateService)
    expect(service).toBeTruthy()
  })
})
