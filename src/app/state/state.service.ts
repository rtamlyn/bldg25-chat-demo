import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { State, selectProductsLoaded, selectProducts, selectAllProducts } from '.'
import { LoadProducts } from './product/product.actions'
import { Product } from '../shared/services/product.model'

@Injectable({
  providedIn: 'root'
})
export class StateService {
  productsLoaded$ = this.store.select(selectProductsLoaded)
  products$ = this.store.select(selectProducts)
  allProducts$ = this.store.select(selectAllProducts)

  constructor(private store: Store<State>) {
    console.log(`state service constructed.`)
  }

  loadProducts(products: Product[]) {
    this.store.dispatch(new LoadProducts({ products }))
  }
}
