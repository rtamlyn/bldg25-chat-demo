import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'

import { SignupComponent } from './signup.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { of } from 'rxjs'
import { AuthService } from '../auth.service'

// providers: [{ provide: AuthService, useClass: MockAuthService }],
class MockAuthService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
  loginFailReason$ = of(null)
  signupFailReason$ = of(null)
  loginSuccess() {}
}

import { MaterialModule } from '../../material/material.module'

describe('SignupComponent', () => {
  let component: SignupComponent
  let fixture: ComponentFixture<SignupComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, MaterialModule, ReactiveFormsModule],
      providers: [{ provide: AuthService, useClass: MockAuthService }],
      declarations: [SignupComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupComponent)
    component = fixture.componentInstance
    component.ngOnInit()
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
  it('email field with invalid email', () => {
    const email = component.signupForm.controls['email']
    email.setValue('testtest.com')
    expect(email.valid).toBeFalsy()
  })
  it('email field wtih valid email still false validity until checked', () => {
    const email = component.signupForm.controls['email']
    email.setValue('test@test.com')
    expect(email.valid).toBeFalsy()
  })
  it('initial password field validity', () => {
    const password = component.signupForm.controls['passwordGroup'].get('password')
    expect(password && password.valid).toBeTruthy()
  })
  it('good password and bad password with blank', () => {
    const password = component.signupForm.controls['passwordGroup'].get('password')
    expect(password).toBeTruthy()
    if (password) {
      password.setValue('abcccde')
      expect(password.valid).toBeTruthy()
      password.setValue('a a')
      expect(password.valid).toBeFalsy()
    }
  })
  it('form invalid with bad email', () => {
    const email = component.signupForm.controls['email']
    email.setValue('test')
    expect(component.signupForm.valid).toBeFalsy()
  })
  it('confirm password valid with mismatched passwords', () => {
    const confirmPassword = component.signupForm.controls['passwordGroup'].get('confirmPassword')
    expect(confirmPassword).toBeTruthy()
    if (confirmPassword) {
      confirmPassword.setValue('test')
      expect(confirmPassword.valid).toBeFalsy()
      expect(confirmPassword.errors && confirmPassword.errors.matchPassword).toBe(true)
    }
  })
})
