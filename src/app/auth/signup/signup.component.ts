import { Component, OnInit, OnDestroy } from '@angular/core'
import { Validators, AbstractControl, FormBuilder } from '@angular/forms'

import { Subscription } from 'rxjs'
import { SignUpInfo } from '../user.model'

import { SignupValidators } from './signup.validators'
import { MatSnackBar } from '@angular/material'
import { environment } from '../../../environments/environment'
import { SignupFailReason } from 'app/auth/state/auth.reducer'
import { AuthService } from '../auth.service'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  USERNAME_MINLENGTH = 3
  PASSWORD_MINLENGTH = 3
  confirmPasswordText = 'Password10'
  hide = true

  signupForm = this.fb.group({
    email: [
      'robert.tamlyn@gmail.com',
      [Validators.required, Validators.email],
      [SignupValidators.shouldBeUniqueEmail]
    ],

    passwordGroup: this.fb.group(
      {
        password: [
          'Password10',
          [
            Validators.required,
            Validators.minLength(this.PASSWORD_MINLENGTH),
            SignupValidators.cannotContainSpace
          ]
        ],
        confirmPassword: ['Password10', [Validators.required]]
      },
      { validator: SignupValidators.passwordMatcher }
    ),

    userName: [
      'Robert',
      [
        Validators.required,
        Validators.minLength(this.USERNAME_MINLENGTH),
        SignupValidators.cannotContainSpace
      ],
      [SignupValidators.shouldBeUniqueUserName]
    ],

    avatarUrl: ['']
  })
  get userNameControl() {
    return this.signupForm.get('userName')
  }
  get emailControl() {
    return this.signupForm.get('email')
  }
  get passwordControl() {
    return this.signupForm.get('passwordGroup.password')
  }
  get confirmPasswordControl() {
    return this.signupForm.get('passwordGroup.confirmPassword')
  }

  _subscriptions: Array<Subscription> = []

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    public snackBar: MatSnackBar
  ) {
    this._subscriptions = [
      // this.stateService.me$.subscribe(user => (this.user = user)),
      // this.stateService.isLoggedIn$.subscribe(isLoggedIn => (this.isLoggedIn = isLoggedIn)),
      this.authService.signupFailReason$.subscribe(reason => {
        if (reason && reason.errors.length) {
          this.openSnackBar(reason, 'dismiss')
          this.authService.clearSignupFail()
        }
      })
    ]
  }

  ngOnInit() {
    if (environment.production) {
      this.signupForm.controls.userName.setValue('')
      this.signupForm.controls.email.setValue('')
      this.signupForm.controls.password.setValue('')
      this.signupForm.controls.confirmPassword.setValue('')
    }
    if (this.userNameControl) console.log(`User name: ${this.userNameControl.value}`)
  }

  ngOnDestroy() {}

  submitSignUp() {
    console.log(this.signupForm.value)
    const credentials: SignUpInfo = this.signupForm.value
    credentials.avatarUrl = credentials.avatarUrl || ''
    if (!credentials.userName) {
      credentials.userName = credentials.email
    }
    this.authService.startSignup(credentials)
  }

  openSnackBar(_reason: SignupFailReason, action: string) {
    this.snackBar.open(_reason.errors[0], action, {
      duration: 4000
    })
  }

  formControlError(input: AbstractControl | null, error: string): boolean {
    if (!input) return false
    else return !!input.errors && input.touched && input.errors[error]
  }

  formControlInvalid(input: AbstractControl | null): boolean {
    if (!input) return false
    else return !!input.errors && input.touched && input.invalid
  }

  checkPasswordError(
    controlName: string = 'confirmPassword',
    errorString: string = 'matchPassword'
  ) {
    const input = this.signupForm.get(`passwordGroup.${controlName}`)
    return this.formControlError(input, errorString)
  }

  formInvalid(): boolean {
    return !this.signupForm.valid
  }
}
