import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { Credentials, User } from '../user.model'
import { Unsubscribe } from 'shared/utils'
import { MatSnackBar } from '@angular/material/snack-bar'
import { SignupValidators } from '../signup/signup.validators'
import { environment } from '../../../environments/environment'

import { ActivatedRoute } from '@angular/router'
import { AuthService } from '../auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  user: User
  isLoggedIn = false
  showPassword = false
  passwordInputType = 'password'
  hide = true
  form = new FormGroup({
    email: new FormControl('admin@gmail.com', [Validators.required, Validators.email]),
    password: new FormControl('Password10', [
      Validators.required,
      SignupValidators.cannotContainSpace
    ])
  })
  returnUrl = '/home'

  get email() {
    return this.form.get('email')
  }
  get password() {
    return this.form.get('password')
  }

  _subscriptions: Array<Subscription> = []

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    if (environment.production) {
      this.form.controls.email.setValue('')
      this.form.controls.password.setValue('')
    }
    this._subscriptions = [
      this.authService.me$.subscribe(user => (this.user = user)),
      this.authService.isLoggedIn$.subscribe(isLoggedIn => (this.isLoggedIn = isLoggedIn)),
      this.authService.loginFailReason$.subscribe(reason => {
        if (reason) {
          this.openSnackBar(reason, 'dismiss')
          this.authService.clearLoginFail()
        }
      })
    ]
    this.returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/'
  }

  @Unsubscribe()
  ngOnDestroy() {
    // this._subscriptions.map((s: Subscription) => {
    //   s.unsubscribe()
    // })
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword
    if (this.showPassword) {
      this.passwordInputType = 'text'
    } else {
      this.passwordInputType = 'password'
    }
  }

  submitSocialLogin(provider: string) {
    console.log(`Social login not setup ${provider}`)
  }

  submitLogin() {
    const credentials: Credentials = this.form.value
    this.authService.startLogin({ credentials })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000
    })
  }
}
