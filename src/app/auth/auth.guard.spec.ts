import { TestBed, inject } from '@angular/core/testing'

import { AuthGuard } from './auth.guard'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { AuthService } from './auth.service'
// providers: [{ provide: AuthService, useClass: MockAuthService }],
class MockAuthService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
  loginFailReason$ = of(null)
  signupFailReason$ = of(null)
  loginSuccess() {}
}
describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [{ provide: AuthService, useClass: MockAuthService }, AuthGuard]
    })
  })

  it('should be created', inject([AuthGuard], (service: AuthGuard) => {
    expect(service).toBeTruthy()
  }))
})
