import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'

import { User, Credentials, SignUpInfo } from './user.model'
import { AuthState } from './state/auth.reducer'
import { Store } from '@ngrx/store'
import {
  LoginSuccess,
  StartLogin,
  StartLogout,
  ClearLoginFail,
  SetReturnUrl,
  StartSignup,
  ClearSignupFail
} from './state/auth.actions'
import {
  selectIsLoggedIn,
  selectMe,
  selectLoginFailReason,
  selectSignupFailReason,
  selectReturnUrl
} from './state'

@Injectable()
export class AuthService {
  me$ = this.authStore.select(selectMe)
  isLoggedIn$ = this.authStore.select(selectIsLoggedIn)
  loginFailReason$ = this.authStore.select(selectLoginFailReason)
  signupFailReason$ = this.authStore.select(selectSignupFailReason)
  returnUrl$ = this.authStore.select(selectReturnUrl)

  constructor(private http: HttpClient, private authStore: Store<AuthState>) {
    console.log('Auth service created.')
    this._relogin()
  }

  _relogin() {
    this.http.get<User>('/api/auth/user-me').subscribe(
      user => {
        if (!!user._id) {
          console.log(`Relogin success`)
          this.authStore.dispatch(new LoginSuccess({ user }))
        } else {
          console.log(`Make sure state has anonymous user.`)
        }
      },
      err => {
        console.log(`Auto relogin not possible: ${err.message}`)
      }
    )
  }
  serverLogout(): Observable<{ success: boolean }> {
    return this.http.post<{ success: boolean }>('/api/auth/logout', null)
  }

  serverSignup(signUpInfo: SignUpInfo) {
    return this.http.post<User>('/api/auth/signup', signUpInfo)
  }

  serverLogin(credentials: Credentials) {
    return this.http.post<User>('/api/auth/login', credentials)
  }

  startLogin(payload: { credentials: Credentials }) {
    this.authStore.dispatch(new StartLogin(payload))
  }

  clearLoginFail() {
    this.authStore.dispatch(new ClearLoginFail())
  }

  startLogout() {
    this.authStore.dispatch(new StartLogout())
  }

  setReturnUrl(returnUrl: string | null) {
    this.authStore.dispatch(new SetReturnUrl(returnUrl))
  }

  startSignup(signUpInfo: SignUpInfo) {
    this.authStore.dispatch(new StartSignup(signUpInfo))
  }

  clearSignupFail() {
    this.authStore.dispatch(new ClearSignupFail())
  }
}
