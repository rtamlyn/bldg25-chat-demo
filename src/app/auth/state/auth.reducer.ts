import { Action } from '@ngrx/store'
import { AuthActions, AuthActionTypes } from './auth.actions'
import { User, ANONYMOUS_USER } from '../user.model'

export const authFeatureKey = 'auth'

export interface AuthState {
  me: User
  loggingOut: boolean
  loggingIn: boolean
  signingUp: boolean
  loginFailReason: string | null
  signupFailReason: SignupFailReason | null
  returnUrl: string | null
}

export interface SignupFailReason {
  errors: string[]
}

export const initialState: AuthState = {
  me: ANONYMOUS_USER,
  loggingOut: false,
  loggingIn: false,
  signingUp: false,
  loginFailReason: null,
  signupFailReason: { errors: [] },
  returnUrl: null
}

export function reducer(state = initialState, action: Action): AuthState {
  const theAction: AuthActions = action as AuthActions
  switch (theAction.type) {
    case AuthActionTypes.StartSignup:
      return { ...state, signingUp: true }

    case AuthActionTypes.LoginFailure:
      return { ...state, loginFailReason: theAction.payload.reason }

    case AuthActionTypes.SignupFailure:
      return { ...state, signupFailReason: theAction.payload.reason }

    case AuthActionTypes.ClearLoginFail:
      return { ...state, loginFailReason: null }

    case AuthActionTypes.ClearSignupFail:
      return { ...state, signupFailReason: { errors: [] } }

    case AuthActionTypes.LoginSuccess:
      return { ...state, me: theAction.payload.user, loggingIn: false, signingUp: false }

    case AuthActionTypes.StartLogin:
      console.log(`Starting Login reducer called`)
      return { ...state, loggingIn: true }
    // return { ...state, loggingIn: true, returnUrl: theAction.payload.returnUrl }

    case AuthActionTypes.SetReturnUrl:
      return { ...state, returnUrl: theAction.payload }

    case AuthActionTypes.StartLogout:
      console.log(`Starting Logout reducer called`)
      return { ...state, loggingOut: true }

    case AuthActionTypes.LogoutSuccess:
      console.log(`Logout success reducer called`)
      return initialState

    default:
      console.log(`Warning: auth default reducer case called: ${theAction.type}.`)
      return state
  }
}
