import { Injectable } from '@angular/core'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { AuthActionTypes, StartLogin, StartLogout, StartSignup } from './auth.actions'
import { of, Observable } from 'rxjs'
import { map, switchMap, catchError, tap } from 'rxjs/operators'
import { AuthService } from 'app/auth/auth.service'
import {
  LogoutSuccess,
  LogoutFailure,
  LoginSuccess,
  LoginFailure,
  SignupFailure
} from './auth.actions'
import { Router } from '@angular/router'

@Injectable()
// TODO: suspect clumsy use of observable from authservice causes this to be invoked when not wanted
// see dev-connect-demo for fix moving this redirect to authservice
export class AuthEffects {
  @Effect({ dispatch: false })
  authRedirectUrl$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    // withLatestFrom(this.stateService.returnUrl$),
    switchMap<LoginSuccess, Observable<boolean>>(loginSuccess => {
      console.log(`intercepted ${loginSuccess.type}`)
      return this.authService.returnUrl$.pipe(
        map(returnUrl => {
          if (!returnUrl) returnUrl = '/home'
          this.router.navigate([returnUrl])
          return true
        })
      )
    })
  )

  @Effect({ dispatch: false })
  authLogoutRedirectUrl$ = this.actions$.pipe(
    ofType(AuthActionTypes.LogoutSuccess),
    tap<LogoutSuccess>(loginSuccess => {
      console.log(`intercepted ${loginSuccess.type}`)
      this.router.navigate(['/home'])
    })
  )

  @Effect()
  authLogout$ = this.actions$.pipe(
    ofType(AuthActionTypes.StartLogout),
    switchMap<StartLogout, Observable<LogoutSuccess | LogoutFailure>>(() => {
      console.log(`submitting logout request to server`)
      return this.authService.serverLogout().pipe(
        map(res => {
          console.log(`logout success: ${res.success}`)
          if (!res.success) return new LogoutFailure({ reason: 'logout rejected by server' })
          return new LogoutSuccess()
        }),
        catchError(_err => of(new LogoutFailure({ reason: 'logout failure' })))
      )
    })
  )

  @Effect()
  authSignup$ = this.actions$.pipe(
    ofType(AuthActionTypes.StartSignup),
    switchMap<StartSignup, Observable<LoginSuccess | SignupFailure>>(startSignup => {
      console.log(`submitting signup request to server`)
      return this.authService.serverSignup(startSignup.payload).pipe(
        map(user => {
          console.log(`login success after signup: ${user.userName}`)
          return new LoginSuccess({ user })
        }),
        catchError(res => {
          console.log(res.error)
          return of(new SignupFailure({ reason: res.error }))
        })
      )
    })
  )

  @Effect()
  authLogin$ = this.actions$.pipe(
    ofType(AuthActionTypes.StartLogin),
    switchMap<StartLogin, Observable<LoginSuccess | LoginFailure>>(startLogin => {
      console.log(`submitting login request to server`)
      return this.authService.serverLogin(startLogin.payload.credentials).pipe(
        map(user => {
          console.log(`login success: ${user.userName}`)
          return new LoginSuccess({ user })
        }),
        catchError(res => {
          console.log(res.error)
          return of(new LoginFailure({ reason: res.error }))
        })
      )
    })
  )

  constructor(
    private actions$: Actions,
    private router: Router,
    private authService: AuthService
  ) {}
}
