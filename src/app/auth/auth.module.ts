import { SignupComponent } from './signup/signup.component'
import { LoginComponent } from './login/login.component'
import { AuthService } from './auth.service'
import { AuthGuard } from './auth.guard'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'

import { MaterialModule } from './material.module'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import * as fromAuth from './state/auth.reducer'
import { AuthEffects } from './state/auth.effects'
import { ProfileComponent } from './profile/profile.component'
import { AuthRoutingModule } from './auth-routing.module'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    AuthRoutingModule,
    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffects])
  ],
  declarations: [LoginComponent, SignupComponent, ProfileComponent],
  providers: [AuthService, AuthGuard]
})
export class AuthModule {}
