import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { HomeComponent } from './core/home/home.component'
import { NotFoundComponent } from './core/not-found/not-found.component'
import { MainDashboardComponent } from './shared/components/main-dashboard/main-dashboard.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'newhome', component: MainDashboardComponent },
  { path: 'home', component: HomeComponent },
  { path: '**', component: NotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
