import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '../../auth/user.model'
import { ShoppingCartService, Cart } from '../../shared/services/shopping-cart.service'
import { Subscription } from 'rxjs'
import { environment } from 'environments/environment'

import { AuthService } from 'app/auth/auth.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  appUser: User
  isLoggedIn = false
  cart: Cart
  development = ''
  collapse = true

  private subscriptions: Subscription[] = []

  constructor(
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService,
    private router: Router
  ) {}

  ngOnInit() {
    this.development = environment.production ? '' : '*** DEVELOPMENT ***'
    this.subscriptions = [
      this.shoppingCartService.cart$.subscribe(cart => (this.cart = cart)),
      this.authService.me$.subscribe(appUser => (this.appUser = appUser)),
      this.authService.isLoggedIn$.subscribe(isLoggedIn => (this.isLoggedIn = isLoggedIn))
    ]
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe())
  }

  logout() {
    this.authService.startLogout()
  }

  private setReturnUrl() {
    const returnUrl = this.router.url
    if (returnUrl !== '/login' && returnUrl !== '/signup') {
      this.authService.setReturnUrl(returnUrl)
    }
  }

  login() {
    this.setReturnUrl()
    this.router.navigate(['/login'])
  }

  signup() {
    this.setReturnUrl()
    this.router.navigate(['/signup'])
  }
}
