import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { FooterComponent } from './footer/footer.component'
import { HomeComponent } from './home/home.component'
import { NavbarComponent } from './navbar/navbar.component'
import { SharedModule } from '../shared/shared.module'

import { MaterialModule } from '../material/material.module'
import { NotFoundComponent } from './not-found/not-found.component'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatIconModule } from '@angular/material/icon'
import { MatMenuModule } from '@angular/material/menu'
import { LayoutModule } from '@angular/cdk/layout'
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    // NgbModule,
    MaterialModule,
    RouterModule.forChild([]),
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule
  ],
  declarations: [NavbarComponent, FooterComponent, HomeComponent, NotFoundComponent],
  exports: [NavbarComponent, FooterComponent]
})
export class CoreModule {}
