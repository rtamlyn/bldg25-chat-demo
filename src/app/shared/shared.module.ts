import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MaterialModule } from '../material/material.module'
import { MainDashboardComponent } from './components/main-dashboard/main-dashboard.component'
import { ProductCardComponent } from './components/product-card/product-card.component'
import { ProductQuantityComponent } from './components/product-quantity/product-quantity.component'
import { OrderService } from './services/order.service'
import { ProductService } from './services/product.service'
import { ShoppingCartService } from './services/shopping-cart.service'
import { RbacAllowDirective } from './directives/rbac-allow.directive'

import { AgmCoreModule } from '@agm/core'

import { MapService } from './services/map.service'

// google maps training api key
// project id: loyal-operation-139212
const apiKey = 'AIzaSyDMq4_9As0HmggDIBrHSXYQQ5sUiRfxSMc'
@NgModule({
  imports: [CommonModule, MaterialModule, AgmCoreModule.forRoot({ apiKey })],
  declarations: [
    MainDashboardComponent,
    ProductCardComponent,
    ProductQuantityComponent,
    RbacAllowDirective
  ],
  exports: [
    MainDashboardComponent,
    ProductCardComponent,
    ProductQuantityComponent,
    RbacAllowDirective
  ],
  providers: [ProductService, OrderService, ShoppingCartService, MapService]
})
export class SharedModule {}
