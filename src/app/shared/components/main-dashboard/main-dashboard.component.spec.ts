import { LayoutModule } from '@angular/cdk/layout'
import { NoopAnimationsModule } from '@angular/platform-browser/animations'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatIconModule } from '@angular/material/icon'
import { MatMenuModule } from '@angular/material/menu'

import { AgmCoreModule, MapsAPILoader } from '@agm/core'

import { MainDashboardComponent } from './main-dashboard.component'
import { MapService } from 'shared/services/map.service'
import { of } from 'rxjs'
import { NO_ERRORS_SCHEMA } from '@angular/core'

class MockMapsAPILoader {
  public load(): Promise<boolean> {
    return new Promise(() => {
      return true
    })
  }
}

class MockMapService {
  getLocation$() {
    return of({ latitude: 0, longitude: 0 })
  }
}

describe('MainDashboardComponent', () => {
  let component: MainDashboardComponent
  let fixture: ComponentFixture<MainDashboardComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MainDashboardComponent],
      providers: [
        { provide: MapService, useClass: MockMapService },
        { provide: MapsAPILoader, useClass: MockMapsAPILoader }
      ],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        AgmCoreModule,
        MatButtonModule,
        MatCardModule,
        MatGridListModule,
        MatIconModule,
        MatMenuModule
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MainDashboardComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should compile', () => {
    expect(component).toBeTruthy()
  })
})
