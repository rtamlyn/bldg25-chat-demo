import { Component, OnInit, OnDestroy } from '@angular/core'
import { map } from 'rxjs/operators'
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout'
import { MapService } from 'shared/services/map.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.scss']
})
export class MainDashboardComponent implements OnInit, OnDestroy {
  /** Based on the screen size, switch from standard to one column per row */
  card0Title = 'Main Router Outlet'
  card0Cols = 2
  card0Rows = 5
  card0Display = false
  lat = ''
  lng = ''

  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 1, rows: 1, content: 'card1' },
          { title: 'Card 2', cols: 1, rows: 1, content: 'card2' },
          { title: 'Card 3', cols: 1, rows: 1, content: 'card3' },
          { title: 'Card 4', cols: 1, rows: 1, content: 'card4' }
        ]
      }

      return [
        { title: 'Card 1', cols: 2, rows: 1, content: 'card1' },
        { title: 'Card 2', cols: 1, rows: 1, content: 'card2' },
        { title: 'Card 3', cols: 1, rows: 2, content: 'card3' },
        { title: 'Card 4', cols: 1, rows: 1, content: 'card4' }
      ]
    })
  )

  private _subscriptions: Subscription[] = []
  constructor(private breakpointObserver: BreakpointObserver, private mapService: MapService) {}

  ngOnInit() {
    this._subscriptions = [
      this.mapService.getLocation$().subscribe(data => {
        this.lat = data.latitude
        this.lng = data.longitude
      })
    ]
  }

  ngOnDestroy() {
    this._subscriptions.forEach(s => s.unsubscribe())
  }
}
