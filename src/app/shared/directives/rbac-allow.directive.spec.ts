import { RbacAllowDirective } from './rbac-allow.directive'

import { TemplateRef, Component } from '@angular/core'
import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { of } from 'rxjs'
import { User } from '../../auth/user.model'

const adminUser: User = {
  email: 'guest',
  _id: '', // keep this falsey
  roles: ['SUPER_USER'],
  userName: 'Guest',
  avatarUrl: ''
}
import { AuthService } from '../../auth/auth.service'
// providers: [{ provide: AuthService, useClass: MockAuthService }],
class MockAuthService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of(adminUser)
  loginFailReason$ = of(null)
  signupFailReason$ = of(null)
  loginSuccess() {}
}
@Component({
  template: `
    <p>First</p>
    <p *appRbacAllow="['SUPER_USER']">Second</p>
    <p *appRbacAllow="['ADMIN', 'SUPER_USER']">Third</p>
    <p *appRbacAllow="['ADMIN']">Fourth</p>
    <p *appRbacAllow="['DUMB_ASS']">Fifth</p>
  `
})
class DirectiveHostComponent {}

describe('RbacAllowDirective', () => {
  let fixture: ComponentFixture<DirectiveHostComponent>
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [DirectiveHostComponent, RbacAllowDirective],
      providers: [TemplateRef, { provide: AuthService, useClass: MockAuthService }]
    }).compileComponents()
  }))

  it('should disallow p as needed', () => {
    fixture = TestBed.createComponent(DirectiveHostComponent)
    fixture.detectChanges()
    const des = fixture.debugElement.queryAll(By.css('p'))
    expect(des[0]).toBeTruthy()
    expect(des[1]).toBeTruthy()
    expect(des[2]).toBeTruthy()
    expect(des.length).toBe(3)
  })
})
