import { TestBed, inject } from '@angular/core/testing'

import { ProductService } from './product.service'
import { HttpClient } from '@angular/common/http'

import { of } from 'rxjs'
import { StateService } from '../../state/state.service'
class MockStateService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
  productsLoaded$ = of({})
}

class MockHttpClient {
  get() {
    return of([])
  }
}
describe('ProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProductService,
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: StateService, useClass: MockStateService }
      ]
    })
  })

  it('should be created', inject([ProductService], (service: ProductService) => {
    expect(service).toBeTruthy()
  }))
})
