import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private accessKey = '7f19b009e2e277bedcd93cbe622f94a7'
  constructor(private http: HttpClient) {}
  getLocation$() {
    return this.http.get<any>(`http://api.ipapi.com/check?access_key=${this.accessKey}`)
  }
}

/*
{
  "success": false,
  "error": {
    "code": 104,
    "type": "monthly_limit_reached",
    "info": "Your monthly API request volume has been reached. Please upgrade your plan."
  }
}
{
  "ip":"2606:a000:111d:81fe:9d1c:37cd:7cd:7194",
  "type":"ipv6",
  "continent_code":"NA",
  "continent_name":"North America",
  "country_code":"US",
  "country_name":"United States",
  "region_code":"NC",
  "region_name":"North Carolina",
  "city":"Apex",
  "zip":"27502",
  "latitude":35.7225,
  "longitude":-78.8408,
  "location":{
    "geoname_id":4452808,
    "capital":"Washington D.C.",
    "languages":[
      {
        "code":"en",
        "name":"English",
        "native":"English"
      }
    ],
    "country_flag":"http:\/\/assets.ipapi.com\/flags\/us.svg",
    "country_flag_emoji":"\ud83c\uddfa\ud83c\uddf8",
    "country_flag_emoji_unicode":"U+1F1FA U+1F1F8",
    "calling_code":"1",
    "is_eu":false
  }
}
*/
