// align models FE: product.service BE: product.api
export interface ProductWoKey {
  title: string
  price: number
  imageUrl: string
  category: string
}
export interface Product extends ProductWoKey {
  id: string
}

export interface Category {
  title: string
  lead: string
  key: string
}
// end align models

export interface Products {
  [id: string]: Product
}
