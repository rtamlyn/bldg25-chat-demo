import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { addKey, values } from 'shared/utils'

import { StateService } from '../../state/state.service'
import { map, filter, switchMap, tap } from 'rxjs/operators'
// import { Observable } from 'rxjs'

import { Product, Category } from './product.model'
@Injectable()
export class ProductService {
  constructor(private http: HttpClient, private storeService: StateService) {
    console.log(`Product service constructor.`)
    this.storeService.productsLoaded$
      .pipe(
        filter(loaded => !loaded),
        switchMap(() => this.loadProductsFromServer())
      )
      .subscribe(products => this.storeService.loadProducts(products))
    // this.getList().subscribe(products => this.storeService.loadProducts(products))
  }

  create(product: Product) {
    return this.http.post<boolean>('/api/product/new-product', product)
  }

  loadProductsFromServer() {
    console.log(`Loading all products from server.`)
    return this.http.get<Product[]>('/api/product/all')
  }

  resetAll() {
    return this.http.post<any>('/api/product/reset-all-products', '')
  }

  // getIndexedList(): Observable<Dictionary<Products>> {
  getIndexedList() {
    return this.storeService.products$
    // return this.getList().pipe(
    //   map(products => {
    //     const newProducts: Products = {}
    //     products.forEach(p => (newProducts[p.id] = p))
    //     return newProducts
    //   })
    // )
  }

  get(productId: string) {
    return this.http.get<Product>('/api/product/' + productId)
  }

  update(productId: string, product: Product) {
    return this.http.put<Product>('/api/product/' + productId, product)
  }

  delete(productId: string) {
    return this.http.delete('/api/product/' + productId)
  }

  getCategories() {
    console.log(`getting cats`)
    return this.http.get<any>('/api/product/categories').pipe(
      tap(cats => console.log(cats)),
      map(addKey),
      map(values),
      map((cats: Category[]) => {
        return cats.sort((a: Category, b: Category) => {
          const x = a.title.toLowerCase()
          const y = b.title.toLowerCase()
          return x < y ? -1 : x > y ? 1 : 0
        })
      })
    )
  }
}
