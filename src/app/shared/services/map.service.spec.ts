import { TestBed } from '@angular/core/testing'
import { HttpClient } from '@angular/common/http'
import { of } from 'rxjs'

import { MapService } from './map.service'

class MockHttpClient {
  get() {
    return of([])
  }
}

describe('MapService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useClass: MockHttpClient }]
    })
  )

  it('should be created', () => {
    const service: MapService = TestBed.get(MapService)
    expect(service).toBeTruthy()
  })
})
