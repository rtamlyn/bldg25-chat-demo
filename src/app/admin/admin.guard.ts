import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'
import { User } from '../auth/user.model'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { AuthService } from 'app/auth/auth.service'

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private authService: AuthService) {}

  canActivate(): Observable<boolean> {
    return this.authService.me$.pipe(
      map((appUser: User) => appUser.roles.includes('ADMIN') || false)
    )
  }
}
