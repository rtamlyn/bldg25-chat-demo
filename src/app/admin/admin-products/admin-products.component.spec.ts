import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AdminProductsComponent } from './admin-products.component'

import { Router } from '@angular/router'
import { DomSanitizer } from '@angular/platform-browser'
import { MaterialModule } from '../../material/material.module'

import { of } from 'rxjs'
import { StateService } from '../../state/state.service'
import { ProductService } from 'shared/services/product.service'

class MockStateService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  loginSuccess() {}
}
class MockProductService {}

class MockRouter {}
class MockDomSanitizer {}

describe('AdminProductsComponent', () => {
  let component: AdminProductsComponent
  let fixture: ComponentFixture<AdminProductsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminProductsComponent],
      imports: [MaterialModule],
      providers: [
        { provide: Router, useClass: MockRouter },
        { provide: DomSanitizer, useClass: MockDomSanitizer },
        { provide: ProductService, useClass: MockProductService },
        { provide: StateService, useClass: MockStateService }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProductsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
