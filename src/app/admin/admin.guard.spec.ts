import { TestBed, inject } from '@angular/core/testing'

import { AdminGuard } from './admin.guard'
import { of } from 'rxjs'
import { AuthService } from '../auth/auth.service'
class MockAuthService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
}

describe('AdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminGuard, { provide: AuthService, useClass: MockAuthService }]
    })
  })

  it('should ...', inject([AdminGuard], (guard: AdminGuard) => {
    expect(guard).toBeTruthy()
  }))
})
