import { Component } from '@angular/core'
import { User } from '../../auth/user.model'
import { OrderService, Order } from '../../shared/services/order.service'
import { Observable } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { AuthService } from 'app/auth/auth.service'

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent {
  orders$: Observable<Order[]>

  constructor(private authService: AuthService, orderService: OrderService) {
    this.orders$ = this.authService.me$.pipe(switchMap((_u: User) => orderService.getMyOrders()))
  }
}
