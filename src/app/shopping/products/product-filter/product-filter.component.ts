import { Component, OnInit, OnDestroy } from '@angular/core'
import { ProductService } from 'shared/services/product.service'
import { Input } from '@angular/core'
import { Category } from '../../../shared/services/product.model'
import { Subscription } from 'rxjs'
@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit, OnDestroy {
  categories: Category[]
  @Input() categoryKey: string

  private _subs: Subscription[] = []
  constructor(private productService: ProductService) {}

  ngOnInit() {
    this._subs = [this.productService.getCategories().subscribe(cats => (this.categories = cats))]
  }

  ngOnDestroy() {
    this._subs.forEach(s => s.unsubscribe())
  }
}
