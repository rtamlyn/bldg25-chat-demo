import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ProductsComponent } from './products.component'
import { ShoppingCartService, Cart } from 'shared/services/shopping-cart.service'
import { ActivatedRoute } from '@angular/router'
import { NO_ERRORS_SCHEMA } from '@angular/core' // needed for product filter component <a>
import { ProductFilterComponent } from './product-filter/product-filter.component'
import { ProductCardComponent } from '../../shared/components/product-card/product-card.component'

import { Observable, of } from 'rxjs'
import { StateService } from '../../state/state.service'

import { ProductService } from 'shared/services/product.service'
import { Category } from 'shared/services/product.model'

class MockStateService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
}

class MockShoppingCartService {
  cart$: Observable<Cart> = of<Cart>(new Cart())
}
class MockActivatedRoute {
  queryParamMap() {
    console.log('query map called')
    return of({
      get: () => of('')
    })
  }
}

const exampleCategories: Category[] = [
  {
    title: 'example cat',
    lead: 'leader',
    key: 'zaq'
  }
]
class MockProductService {
  getCategories(_key: string) {
    return of(exampleCategories)
  }
}

describe('ProductsComponent', () => {
  let component: ProductsComponent
  let fixture: ComponentFixture<ProductsComponent>

  beforeEach(async(() => {
    // async waits for 5 seconds for promises to complete
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ProductService, useClass: MockProductService },
        { provide: ShoppingCartService, useClass: MockShoppingCartService },
        { provide: ActivatedRoute, useClass: MockActivatedRoute }
      ],
      declarations: [ProductsComponent, ProductFilterComponent, ProductCardComponent]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ProductsComponent)
        component = fixture.componentInstance
      })
  }))

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ProductsComponent)
  //   component = fixture.componentInstance
  //   fixture.detectChanges()
  // })

  it('should create', () => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  })

  afterEach(() => {
    fixture.destroy()
  })
})
