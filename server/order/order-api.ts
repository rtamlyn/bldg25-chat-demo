import { MongoProductDatabase } from '../database/mongo-products'
import { User } from '../database/user-db/user'
import { RequestApp, ResponseApp } from '../express-app'

export interface Order {
  userId: string
  email: string
  password: string
  userName: string
}

export function postOrder(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  const order: Order = req.body
  const user: User = res.locals.user
  db.createOrder(order, user._id || 'unknown').then(result => {
    if (result) res.status(200).json({ success: result })
    else res.status(403).json({ success: false, reason: 'unable to place order' })
  })
}

export function getMyOrders(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  const userId: string = res.app.locals.userId
  db.getOrdersById(userId)
    .then(orders => {
      res.status(200).json(orders)
    })
    .catch(_err => {
      res.status(403).json([])
    })
}

export function getAllOrders(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  db.getAllOrders().then(orders => {
    if (orders) {
      res.send(200).json({ orders })
    } else {
      res.send(400).json(`success: false`)
    }
  })
}
