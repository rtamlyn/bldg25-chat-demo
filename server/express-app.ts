import express from 'express'
import { Databases } from './connect-dbs'
import { User } from './database/user-db/user'

export interface ExpressApp extends express.Application {
  locals: { dbs: Databases }
}

export interface RequestApp extends express.Request {
  app: ExpressApp
}

export interface ResponseApp extends express.Response {
  locals: { user: User }
}
