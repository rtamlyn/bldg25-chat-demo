import { NextFunction } from 'express'
import { ResponseApp } from '../../express-app'

export function checkIfAuthenticated(_req: any, res: ResponseApp, next: NextFunction) {
  if (res.locals.user) {
    next()
  } else {
    res.status(401).send({})
  }
}

export function checkForRelogin(_req: any, res: ResponseApp, next: NextFunction) {
  if (res.locals.user) {
    next()
  } else {
    // CONSIDER: consider sending anonymouse user
    res.status(200).send({ message: 'Please login' })
  }
}
