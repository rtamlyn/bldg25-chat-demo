import { getUserFromJwt } from '../security'
import { NextFunction, Request } from 'express'
import { RequestApp, ResponseApp } from '../../express-app'
import { parse } from 'cookie'

// CONSIDER: use getUserFromReq instead of duplicate code
export function addUserToRes(req: RequestApp, res: ResponseApp, next: NextFunction) {
  const jwt = req.cookies['SESSIONID']
  if (jwt) {
    getUserFromJwt(jwt)
      .then(user => {
        if (user) res.locals.user = user
        next()
      })
      .catch(_err => {
        console.log(`Did not find user in request. Continuing anyway for now...`)
        next()
      })
  } else {
    next()
  }
}

// CONSIDER: should this use RequestApp?
export function getUserFromReq(req: Request) {
  const cookies = parse(req.headers.cookie || '')
  const jwt = cookies['SESSIONID']
  return getUserFromJwt(jwt)
}
