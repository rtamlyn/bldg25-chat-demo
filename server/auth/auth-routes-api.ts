import express from 'express'
import {
  createUser,
  login,
  logout,
  getJwtUser,
  getUserByEmail,
  deleteUser,
  cleanChat
} from './auth-api'
import { checkIfAuthenticated, checkForRelogin } from './mware/authentication'
import { checkIfAuthorized } from './mware/authorization'
import { checkCsrfToken } from './mware/csrf'

export const authRouter = express.Router()

authRouter
  .post('/signup', createUser)
  .post('/login', login)
  .post('/logout', checkIfAuthenticated, checkCsrfToken, logout)
  .post('/clean', checkIfAuthenticated, checkCsrfToken, checkIfAuthorized(['ADMIN']), cleanChat)
  .get('/user-me', checkForRelogin, getJwtUser) // used by relogin so cannot check csrftoken
  .get('/user/:email', checkIfAuthenticated, checkCsrfToken, getUserByEmail)
  // .put('/user/:id', checkIfAuthenticated, saveUser)
  .delete(
    '/user/:email',
    checkIfAuthenticated,
    checkCsrfToken,
    checkIfAuthorized(['ADMIN']),
    deleteUser
  )
