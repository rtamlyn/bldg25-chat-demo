// const crypto = require('crypto')
import crypto from 'crypto'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import { User } from '../database/user-db/user'
import { serverConfig } from '../server.config'
import { jsonParseResolve } from '../jsonParse'

import { getLogger } from 'log4js'
const log = getLogger('security')

export function getRandomBytes(bits: number) {
  return new Promise<Buffer>((resolve, reject) => {
    crypto.randomBytes(bits, (err: Error | null, buf: Buffer) => {
      if (err) return reject(err)
      else return resolve(buf)
    })
  })
}

const RSA_PRIVATE_KEY = process.env.RSA_PRIVATE_KEY || fs.readFileSync('./server/keys/private.key')
const RSA_PUBLIC_KEY = process.env.RSA_PUBLIC_KEY || fs.readFileSync('./server/keys/public.key')

const SESSION_DURATION_SECS = Math.round(serverConfig.TOKEN_AGE_MS / 1000)
log.warn(`session duration is set to: ${SESSION_DURATION_SECS}`)
const CHAT_SESSION_DURATION_SECS = Math.round(serverConfig.CHAT_TOKEN_AGE_MS / 1000)
log.warn(`chat token duration is set to: ${CHAT_SESSION_DURATION_SECS}`)

export function createSessionToken(user: User) {
  const rUser = { ...user }
  const options: jwt.SignOptions = {
    algorithm: 'RS256',
    expiresIn: SESSION_DURATION_SECS,
    subject: JSON.stringify(rUser)
  }
  const token = jwt.sign(
    {
      roles: user.roles
    },
    RSA_PRIVATE_KEY,
    options
  )
  return token
}

export async function createCsrfToken() {
  const token = await getRandomBytes(32).then((bytes: Buffer) => bytes.toString('hex'))
  return token
}

export function createChatToken(user: User) {
  const rUser = { ...user }
  const options: jwt.SignOptions = {
    algorithm: 'RS256',
    expiresIn: CHAT_SESSION_DURATION_SECS,
    subject: JSON.stringify(rUser)
  }
  const token = jwt.sign({}, RSA_PRIVATE_KEY, options)
  return token
}

export async function getChatUserFromJwt(chatToken: string): Promise<User> {
  return new Promise<User>((resolve, reject) => {
    jwt.verify(chatToken, RSA_PUBLIC_KEY, (err, result) => {
      if (err) {
        return reject(err)
      } else {
        if (typeof result === 'string') return reject('Error: recieved string from jwt.verify')
        const decodedToken = result as any
        const timeLeftSecs = decodedToken.exp - Date.now() / 1000
        if (timeLeftSecs < 2) {
          console.log(
            `Expiration of ${timeLeftSecs} is less than 2 secs. Consider reject OR find other way to alert front end.`
          )
        } else {
          console.log(`Expiration of ${timeLeftSecs} is more than 2 secs.`)
        }
        jsonParseResolve<User>(decodedToken.sub, resolve, reject)
      }
    })
  })
}

export async function getUserFromJwt(token: string): Promise<User> {
  return new Promise<User>((resolve, reject) => {
    jwt.verify(token, RSA_PUBLIC_KEY, (err, result) => {
      if (err) {
        return reject(err)
      } else {
        if (typeof result === 'string') return reject('Error: recieved string from jwt.verify')
        const decodedToken = result as any
        const timeLeftHrs = (decodedToken.exp - global.Date.now() / 1000) / 3600
        if (timeLeftHrs < 12) {
          log.warn(
            `Expiration of ${timeLeftHrs} is less than 12 hrs. Consider reject OR find other way to alert front end.`
          )
        }
        jsonParseResolve<User>(decodedToken.sub, resolve, reject)
      }
    })
  })
}
