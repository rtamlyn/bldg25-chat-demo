import { User } from './user'
import { Observable } from 'rxjs'

export function fromPromise<T>(p: Promise<T>): Observable<T> {
  return new Observable(s => {
    p.then(r => {
      s.next(r)
      s.complete()
    }).catch(e => s.error(e))
  })
}

export function fromPromiseNull<T>(p: Promise<T | null>): Observable<T> {
  return new Observable(s => {
    p.then(r => {
      if (!r) {
        s.error(`Not found from promise null`)
      } else {
        s.next(r)
        s.complete()
      }
    }).catch(e => s.error(e))
  })
}

export abstract class UserDatabase {
  abstract flushDb(): Promise<boolean>

  // CONSIDER: Dbclean needs this promise version; Change to stream version?
  getUserById(userId: string): Promise<User | null> {
    return this.getUserById$(userId)
      .toPromise()
      .catch(_e => null)
  }

  abstract getUserById$(userId: string): Observable<User>
  abstract getUserByEmail$(email: string): Observable<User>
  abstract getUserPwdDigest$(email: string): Observable<string>
  abstract createUser$(
    info: User<null>,
    passwordDigest: string
  ): Observable<{ success: boolean; user: User; errors: string[] }>
  abstract deleteUser$(email: string): Observable<boolean>
}
