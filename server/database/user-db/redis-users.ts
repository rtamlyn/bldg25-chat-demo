import { RedisClient } from 'redis'
import { User } from './user'
import { UserDatabase } from './user-db'
import { Observable, Subscriber, forkJoin, of } from 'rxjs'
import { switchMap, map } from 'rxjs/operators'
import { jsonParseNull } from '../../jsonParse'

const USER_EMAIL = 'user:email'

const USER_COLLECTION = 'users'
const PASSWORD_COLLECTION = 'passwords'

export class RedisUserDatabase extends UserDatabase {
  constructor(private client: RedisClient) {
    super()
  }

  flushDb() {
    console.warn(`Redis user database is being flushed`)
    return Promise.all([
      this.client.del(USER_COLLECTION),
      this.client.del(PASSWORD_COLLECTION),
      this.client.del(USER_EMAIL)
    ]).then(results => results.every(r => r))
  }

  close() {
    return this.client.quit()
  }

  getUserAppId$(email: string): Observable<string> {
    email = email.toLocaleLowerCase()
    return this._getHashItem$<string>(USER_EMAIL, email)
  }

  getUserById$(appUserId: string): Observable<User> {
    return this._getHashItem$<User>(USER_COLLECTION, appUserId)
  }

  getUserByEmail$(email: string): Observable<User> {
    email = email.toLocaleLowerCase()
    return this.getUserAppId$(email).pipe(switchMap((appId: string) => this.getUserById$(appId)))
  }

  getUserPwdDigest$(email: string): Observable<string> {
    email = email.toLocaleLowerCase()
    return this._getHashItem$<string>(PASSWORD_COLLECTION, email)
  }

  // createUser$(user: User<null>, passwordDigest: string): Observable<User> {
  createUser$(
    info: User<null>,
    passwordDigest: string
  ): Observable<{ success: boolean; user: User; errors: string[] }> {
    const errors: string[] = []
    const user: User = { ...info, _id: info.email }
    const response = { success: false, user, errors }
    return this._hashItemExists$(USER_EMAIL, user.email).pipe(
      switchMap(_exists => {
        if (_exists) {
          response.success = false
          response.errors.push(`User with email ${info.email} already exists`)
          return of(response)
        }
        return this.saveUser$(user.email, user, passwordDigest).pipe(
          map(saveSuccess => {
            if (saveSuccess) {
              response.success = true
              return response
            } else {
              response.success = false
              response.errors.push(`Server error while saving user ${info.email}`)
              return response
            }
          })
        )
      })
    )
  }

  // CONSIDER: having saveUser return create response
  private saveUser$(_id: string, user: User, passwordDigest: string): Observable<boolean> {
    return forkJoin([
      this._setHashItem$(USER_COLLECTION, _id, user),
      this._setHashItem$(PASSWORD_COLLECTION, user.email, passwordDigest),
      this._setHashItem$(USER_EMAIL, user.email, _id)
    ]).pipe(
      map(
        results => results.every(r => r)
        // if (!results.every(r => r)) throwError(`Server error creating valid new user`)
        // return user
      )
    )
  }

  deleteUser$(email: string): Observable<boolean> {
    email = email.toLocaleLowerCase()
    return this.getUserAppId$(email).pipe(
      switchMap(appId => {
        // if (!appId) return of([false])
        return forkJoin([
          this._delHashItem$(USER_COLLECTION, appId),
          this._delHashItem$(PASSWORD_COLLECTION, email),
          this._delHashItem$(USER_EMAIL, email)
        ])
      }),
      map(results => results.every(r => !!r))
    )
  }

  //
  // Start Redis Access Methods
  //
  private _hashItemExists$(key: string, field: string): Observable<boolean> {
    return new Observable<boolean>((s: Subscriber<boolean>) => {
      this.client.hexists(key, field, (err, n) => {
        if (!!err) {
          s.error(`Error retrieving hash item: ${err}`)
        } else {
          s.next(n > 0)
          s.complete()
        }
      })
    })
  }

  private _getHashItem$<T>(key: string, field: string): Observable<T> {
    return new Observable<T>((s: Subscriber<T>) => {
      this.client.hget(key, field, (err, item) => {
        if (!!err) {
          s.error(`Error retrieving hash item: ${err}`)
        } else if (!item) {
          s.error(`Not found: ${key} ${field}`)
        } else {
          const nextItem = jsonParseNull<T>(item)
          if (nextItem) {
            s.next(nextItem)
            s.complete()
          } else s.error(`hash item not in JSON format`)
        }
      })
    })
  }

  private _setHashItem$(key: string, field: string, value: any): Observable<boolean> {
    return new Observable((s: Subscriber<boolean>) => {
      this.client.hset(key, field, JSON.stringify(value), err => {
        if (!!err) {
          console.log(`Error setting hash item ${key} ${field} ${value}: ${err}`)
          s.error(`Error setting hash item ${key} ${field} ${value}: ${err}`)
        } else {
          s.next(true)
          s.complete()
        }
      })
    })
  }

  private _delHashItem$(key: string, field: string | string[]): Observable<number> {
    return new Observable<number>((s: Subscriber<number>) => {
      this.client.hdel(key, field, (err, nItems) => {
        if (!!err) {
          s.error(`Error deleting hash item ${key} ${field}: ${err}`)
        } else if (!nItems) {
          s.error(`hash item to delete not found: ${key} ${field}`)
        } else {
          s.next(nItems)
          s.complete()
        }
      })
    })
  }

  //
  // End Redis Access Methods
  //
}
