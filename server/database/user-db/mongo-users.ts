import { Db, Collection, MongoClient, ObjectId } from 'mongodb'
import { User } from './user'
import { Password } from './user'
import { UserDatabase, fromPromiseNull, fromPromise } from './user-db'
import { Observable } from 'rxjs'

function addUserId(dbUser: User<ObjectId> | null): User | null {
  if (!dbUser) {
    return null
  }
  const _id = dbUser._id ? dbUser._id.toHexString() : 'id missing in dbUser'
  return { ...dbUser, _id }
}

// function makeUser(dbUser: User<ObjectId>): User {
//   const _id = dbUser._id ? dbUser._id.toHexString() : 'id missing in dbUser'
//   return { ...dbUser, _id }
// }

// function insertResult(result: any) {
//   return result.insertedCount === 1
// }

const USER_COLLECTION = 'users'
const PASSWORD_COLLECTION = 'passwords'

export class MongoUserDatabase extends UserDatabase {
  private db: Db
  private usersCollection: Collection<User<ObjectId>>
  private passwordsCollection: Collection<Password>

  constructor(private client: MongoClient, dbName: string) {
    super()
    console.log('Instance of mongo user database class created.')
    this.db = this.client.db(dbName)
    this.usersCollection = this.db.collection<User<ObjectId>>(USER_COLLECTION)
    this.passwordsCollection = this.db.collection<Password>(PASSWORD_COLLECTION)
    this._createIndexes()
  }

  private _createIndexes() {
    this.usersCollection.createIndexes([{ key: { email: -1 }, unique: true }]).then(result => {
      if (!result.ok) console.log(`Indexes created for users ok: ${result.ok}`)
    })
    this.passwordsCollection.createIndexes([{ key: { email: -1 }, unique: true }]).then(result => {
      if (!result.ok) console.log(`Index created for passwords ok: ${result.ok}`)
    })
  }

  flushDb() {
    const flushes = [this.usersCollection.deleteMany({}), this.passwordsCollection.deleteMany({})]
    return Promise.all(flushes).then(results => {
      const success = results.every(r => !!r.result.ok)
      console.log(`Database flushed`)
      return success
    })
  }

  private _getUserById(userId: string): Promise<User | null> {
    return this.usersCollection.findOne({ _id: new ObjectId(userId) }).then(addUserId)
  }

  getUserById$(userId: string): Observable<User> {
    return fromPromiseNull(this._getUserById(userId))
  }

  getUserByEmail(email: string): Promise<User | null> {
    email = email.toLocaleLowerCase()
    return this.usersCollection
      .findOne({ email })
      .then(addUserId)
      .catch(_e => null)
  }

  getUserByEmail$(email: string): Observable<User> {
    return fromPromiseNull(this.getUserByEmail(email))
  }

  getUserPwdDigest$(email: string): Observable<string> {
    email = email.toLocaleLowerCase()
    return fromPromiseNull(
      this.passwordsCollection
        .findOne({ email })
        .then(password => (password ? password.passwordDigest : null))
    )
  }

  // createUser$(user: User<null>, passwordDigest: string): Observable<User> {
  //   return this._createUser$(user, passwordDigest).pipe(map(u => makeUser(u)))
  // }

  // abstract createUser$(
  //   info: User<null>,
  //   passwordDigest: string
  // ): Observable<{ success: boolean; user: User; errors: string[] }>
  createUser$(
    info: User<null>,
    passwordDigest: string
  ): Observable<{ success: boolean; user: User<string>; errors: string[] }> {
    const errors: string[] = []
    const userToSave: User<any> = { ...info, _id: undefined }
    const response = { success: false, user: { ...info, _id: '' }, errors }
    const p = this.usersCollection
      .insertOne(userToSave)
      .then(
        (r: {
          result: { ok: any }
          insertedCount: number
          insertedId: { toHexString: () => string }
        }) => {
          if (!r.result.ok || r.insertedCount !== 1) {
            // throw Error(`Unable to save user ${info.email}. Inserted count: ${r.insertedCount}`)
            response.errors.push(
              `Unable to save user ${info.email}. Inserted count: ${r.insertedCount}`
            )
            // return response
          } else {
            response.user._id = r.insertedId.toHexString()
            response.success = true
            // return response
          }
        }
      )
      .then(() => {
        if (response.success) {
          return this.passwordsCollection
            .insertOne({ email: info.email, passwordDigest })
            .then(r => {
              if (r.insertedCount !== 1) {
                response.success = false
                errors.push(`Unable to save password`)
                // TODO: should delete user if unable to save password
                console.error(
                  `Server error User ${info.email} saved but password digest failed to save.`
                )
              }
              return response
            })
        }
        return Promise.resolve(response)
      })
      .catch((_err: any) => {
        errors.push(`Unable to save user: ${info.email}. Already exists`)
        return response
      })
    return fromPromise(p)
  }

  // createUserOld(user: User<null>, passwordDigest: string): Promise<boolean> {
  //   user.email = user.email.toLocaleLowerCase()
  //   return Promise.all([
  //     this.usersCollection.insertOne(user).then(insertResult),
  //     this.passwordsCollection
  //       // OLDIXME: this could overwrite password of existing user
  //       .updateOne({ email: user.email }, { $set: { passwordDigest } }, { upsert: true })
  //       .then(r => {
  //         return r.upsertedCount === 1 || r.modifiedCount === 1
  //       })
  //   ]).then(results => results.every(r => r))
  // }

  deleteUser$(email: string): Observable<boolean> {
    email = email.toLocaleLowerCase()
    return fromPromise(
      Promise.all([
        this.usersCollection.deleteOne({ email }),
        this.passwordsCollection.deleteOne({ email })
      ]).then(results => results.every(r => r.deletedCount === 1))
    )
  }
}
