// tslint:disable:max-line-length
// import { User } from '../auth/models/user'
import { UserDatabase } from './user-db'
import {
  createClients,
  createChatMongoDatabase,
  createUserMongoDatabase,
  createChatRedisDatabase,
  createUserRedisDatabase
} from '../connect-dbs'

import { serverConfig } from '../server.config'
import { ChatDatabase } from 'bldg25-chat-server'
const mongoUrl = serverConfig.MONGO_URL

// const catUrl = 'http://localhost:4200/assets/cat.jpg'
// const dogUrl = 'http://localhost:4200/assets/dog.jpg'

// Password10
// const passwordDigest =
//   '$argon2i$v=19$m=4096,t=3,p=1$EAOnDLilKQyKF3lDVxiSoA$YQfnRYqxh62mwk5Qo1EmSZyNcrP6G+ZcYPpFKM690AI'
// const users: User<null>[] = [
//   {
//     email: 'student@gmail.com',
//     userName: 'MsStudent',
//     roles: ['STUDENT'],
//     avatarUrl: dogUrl
//   },
//   {
//     email: 'admin@gmail.com',
//     userName: 'MrAdmin',
//     roles: ['STUDENT', 'ADMIN'],
//     avatarUrl: catUrl
//   },
//   {
//     userName: 'Rob',
//     roles: ['STUDENT'],
//     email: 'rob@rob.com',
//     avatarUrl: catUrl
//   },
//   {
//     email: 'Heath44@hotmail.com',
//     roles: ['STUDENT'],
//     userName: 'Aaron Moore',
//     avatarUrl: ''
//   },
//   {
//     email: 'Gideon9@yahoo.com',
//     roles: ['STUDENT'],
//     userName: 'Yvonne Conroy Mrs.',
//     avatarUrl: ''
//   },
//   {
//     email: 'Laney_Huels@hotmail.com',
//     roles: ['STUDENT'],
//     userName: 'Laron Padberg',
//     avatarUrl: ''
//   },
//   {
//     email: 'Aletha.Labadie@hotmail.com',
//     roles: ['STUDENT'],
//     userName: 'Dr. Maryam Spinka',
//     avatarUrl: ''
//   },
//   {
//     email: 'Rogelio24@hotmail.com',
//     roles: ['STUDENT'],
//     userName: 'Kiley Baumbach',
//     avatarUrl: ''
//   },
//   {
//     email: 'Yazmin.Heidenreich97@gmail.com',
//     roles: ['STUDENT'],
//     userName: 'Hollis MacGyver',
//     avatarUrl: ''
//   },
//   {
//     email: 'Deon_Heaney@gmail.com',
//     roles: ['STUDENT'],
//     userName: 'Axel McLaughlin',
//     avatarUrl: ''
//   }
// ]

// const victim = 'Deon_Heaney@gmail.com'

console.log(`Using mongoUrl: ${mongoUrl}`)

createClients()
  .then(([mongoClient, redisClient]) => {
    if (mongoClient && mongoClient.isConnected()) {
      const db = createUserMongoDatabase(mongoClient)
      const chatDb: ChatDatabase = createChatMongoDatabase(mongoClient)
      chatDb.flushDb().then(result => {
        console.log(`Mongo Chat data base flushed: ${result}`)
      })
      runPreload(db).then(() => {
        mongoClient.close().then(() => {
          console.log(`Mongo client closed`)
          // return true
        })
      })
    }
    if (redisClient) {
      const db = createUserRedisDatabase(redisClient)
      const chatDb: ChatDatabase = createChatRedisDatabase(redisClient)
      chatDb.flushDb().then(result => {
        console.log(`Redis Chat data base flushed: ${result}`)
      })
      runPreload(db).then(() => {
        redisClient.quit()
        console.log(`Redis client closed`)
        // return true
      })
    }
    return true
  })
  .then(finsihed => console.log(`Finished creating clients: ${finsihed}`))
  .catch(err => {
    console.log(`Error while connecting: ${err}`)
  })

function runPreload(db: UserDatabase) {
  return db.flushDb().then(flushResult => {
    console.log(`Flush result: ${flushResult}`)
    return flushResult
  })
}

// function runPreloadWithUsers(db: UserDatabase) {
//   return db.flushDb().then(flushResult => {
//     console.log(`Flush result: ${flushResult}`)
//     const userCreates = users.map(userWoId => db.createUser(userWoId, passwordDigest))
//     return Promise.all(userCreates)
//       .then(userCreateResults => {
//         const success = userCreateResults.every(result => result)
//         console.log('users loaded: ', success)
//         return success
//       })
//       .then(success => {
//         console.log(`${victim} deleted: ${success}`)
//         return success
//       })
//       .then(_number => {
//         return db.getUserByEmail('rob@rob.com')
//       })
//       .then(user => {
//         console.log('Found rob: ', user ? user.userName === 'Rob' : false)
//       })
//       .catch(err => {
//         console.log(`not all users loaded: ${err}`)
//       })
//   })
// }
