import { RedisClient } from 'redis'
import { CategoryDatabase, Categories, CATEGORY_COLLECTION } from './categories-db'
import { jsonParse } from '../../jsonParse'

import { getLogger } from 'log4js'
const log = getLogger('redis-categories')

export class RedisCategoryDatabase extends CategoryDatabase {
  constructor(private client: RedisClient) {
    super()
    log.info('Instance of redis category database created.')
  }

  private getItem(key: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.client.get(key, (_err, item) => {
        if (_err) reject(_err)
        resolve(item)
      })
    })
  }

  private setItem(key: string, value: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.client.set(key, value, (_err, _ok) => {
        if (_err) reject(_err)
        resolve(true)
      })
    })
  }

  saveAllCategories(cats: Categories): Promise<boolean> {
    return this.setItem(CATEGORY_COLLECTION, JSON.stringify(cats))
  }

  getAllCategories(): Promise<Categories> {
    return this.getItem(CATEGORY_COLLECTION).then((sCats: string) => {
      return jsonParse(sCats)
    })
  }
}
