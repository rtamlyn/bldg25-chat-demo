import { Db, Collection, MongoClient, ObjectId } from 'mongodb'

import { CategoryDatabase, Categories, CATEGORY_COLLECTION } from './categories-db'
import { Category } from '../../product/product-model'
interface DbCategory extends Category {
  _id: ObjectId
}

export class MongoCategoryDatabase extends CategoryDatabase {
  private db: Db
  private categoryCollection: Collection<Category>

  constructor(private client: MongoClient, dbName: string) {
    super()
    // console.log('Instance of mongo category database created.')
    this.db = this.client.db(dbName)
    this.categoryCollection = this.db.collection<Category>(CATEGORY_COLLECTION)
    // this.resetCategories().then(() => console.log(`Categories reset. Remove this code.`))
  }

  flushDb() {
    const flushes = [this.categoryCollection.deleteMany({})]
    return Promise.all(flushes).then(results => {
      const success = !!results[0].result.ok
      console.log(`Database flushed`)
      return success
    })
  }

  saveAllCategories(cats: Categories): Promise<boolean> {
    // const catsArray: DbCategory[] = Object.keys(cats).map(key => ({
    const catsArray: DbCategory[] = Object.keys(cats).map(key => ({
      _id: new ObjectId(key),
      ...cats[key]
    }))
    return this.categoryCollection
      .deleteMany({})
      .then(deleteResult => {
        if (!deleteResult) {
          console.error(`Failed to delete all categories. Aborting category reset.`)
        } else {
          console.log(`All categories deleted successfully`)
        }
        return this.categoryCollection.insertMany(catsArray)
      })
      .then(insertResult => {
        return insertResult.insertedCount === catsArray.length
      })
  }

  getAllCategories(): Promise<Categories> {
    return this.categoryCollection
      .find({})
      .toArray()
      .then(cats => {
        const categories: Categories = {}
        cats.forEach(cat => (categories[cat.key] = cat))
        return categories
      })
  }
}
