import { Category } from '../../product/product-model'
import { categoriesPreload } from '../reset-app-db'

export const CATEGORY_COLLECTION = 'categories'

export interface Categories {
  [key: string]: Category
}

export abstract class CategoryDatabase {
  abstract saveAllCategories(cats: Categories): Promise<boolean>
  abstract getAllCategories(): Promise<Categories>
  resetCategories(): Promise<Categories> {
    return this.saveAllCategories(categoriesPreload).then(success => {
      console.log(`All cats reset: ${success}`)
      return categoriesPreload
    })
  }
}
