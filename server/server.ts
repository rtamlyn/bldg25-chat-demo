import express, { Request } from 'express'
import cookieParser from 'cookie-parser'
import { parse } from 'cookie'

import http from 'http'
import https from 'https'
import fs from 'fs'

import { addUserToRes } from './auth/mware/get-user'

import { authRouter } from './auth/auth-routes-api'
import { productRouter } from './product/product-routes'
import { shoppingCartRouter } from './shopping-cart/shopping-cart-routes'
import { orderRouter } from './order/order-routes'
import bodyParser from 'body-parser'

// necessary imports from bldg25 chat server package
import { ChatDatabase, ChatWebSocketServer, clean } from 'bldg25-chat-server'

import { serverConfig } from './server.config'
import { addDatabases } from './connect-dbs'
import { ExpressApp } from './express-app'

import { getLogger } from 'log4js'
import { getChatUserFromJwt } from './auth/security'
const log = getLogger('server')
log.level = 'trace'

const app: ExpressApp = express()

app.use(cookieParser())
app.use(addUserToRes)
app.use(bodyParser.json())

// for serving in production
if (process.env.PROD || serverConfig.prod) {
  const webDir = `${__dirname}`
  log.info(`Serving app from ${webDir}`)
  app.use(express.static(webDir))
}

// REST API
app.use('/api/product', productRouter)
app.use('/api/shopping-carts', shoppingCartRouter)
app.use('/api/auth', authRouter)
app.use('/api/order', orderRouter)

// heroku deployment and prod mode
if (process.env.PROD || serverConfig.prod) {
  log.info(`Running in prod mode: ${serverConfig.HOST_URL}:${serverConfig.PORT}`)
  // gives response when user refreshes some random url in angular
  app.all('*', (_req: any, res: any) => {
    res.status(200).sendFile(__dirname + '/index.html')
  })
}

const server = serverConfig.secure
  ? https.createServer(
      {
        key: fs.readFileSync('./server/keys/server.key'),
        cert: fs.readFileSync('./server/keys/server.crt')
        // key: fs.readFileSync('./server/keys/key.pem'),
        // cert: fs.readFileSync('./server/keys/cert.pem')
      },
      app
    )
  : http.createServer(app)

addDatabases(app).then(success => {
  if (success) runServer(app.locals.dbs.chatDb)
  else {
    console.error('Unable to connect to databases')
  }
})
async function runServer(chatDb: ChatDatabase) {
  const port = serverConfig.PORT
  //
  // *** Optionally clean data base
  //
  // CONSIDER: setup periodic cleaning
  // TODO: Check if this is deleting legitimate users
  const cleanResult = await clean(chatDb, email => {
    return app.locals.dbs.userDb
      .getUserByEmail$(email)
      .toPromise()
      .then(user => !!user)
      .catch(_e => false)
  })
  if (!cleanResult) log.warn(`Clean result: ${cleanResult}`)
  else log.info(`Clean result: ${cleanResult}`)
  //
  // *** Chat server must be added to the express server as follows:
  //
  const wss = new ChatWebSocketServer({ noServer: true }, chatDb, {
    CHAT_LOGGING: false
  })
  log.info(`info: Created chat wss with max listener count: ${wss.getMaxListeners()}`)
  server.on('upgrade', (req: Request, socket: any, head: any) => {
    // Chat server needs user with min props: email
    const cookies = parse(req.headers.cookie || '')
    const chatToken = cookies['CHAT-TOKEN']
    getChatUserFromJwt(chatToken)
      .then(tokenChatUser => {
        wss.handleUpgrade(req, socket, head, ws => {
          wss.upgradeWebSocket(ws, tokenChatUser).catch(err => {
            console.warn(`Bad connection attemmpt with ${tokenChatUser.email}`)
            console.warn(err)
            socket.destroy()
          })
        })
      })
      .catch(_e => {
        console.log(`Invalid creds to connect to chat web socket`)
        socket.destroy()
      })
  })

  // server.on('upgrade', async (req: Request, socket: any, head: any) => {
  //   getUserFromReq(req)
  //     .then(user => {
  //       wss.handleUpgrade(req, socket, head, ws => {
  //         log.trace(`User ${user.email} has connected with ChatWebSocket`)
  //         wss.emit('connection', ws, req, user) // Chat server needs user with email and appId or _id
  //       })
  //     })
  //     .catch(_e => {
  //       log.error(`Websocket connection attempted without valid user`)
  //       socket.destroy()
  //     })
  // })

  server.listen(port, () => {
    log.info(`Server running at port: ${port}`)
  })
}
