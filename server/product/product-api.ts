// import { redisdb } from '../database/redis'
import { RequestApp, ResponseApp } from '../express-app'

import { ObjectId } from 'mongodb'
import { MongoProductDatabase } from '../database/mongo-products'
import { categoriesPreload } from '../database/reset-app-db'

import { take } from 'rxjs/operators'

import { ProductWoKey, Product } from './product-model'

export interface DbProduct extends ProductWoKey {
  _id?: ObjectId
}

export function resetAllProducts(req: RequestApp, res: ResponseApp) {
  const db = req.app.locals.dbs.productDb
  const dbSC = req.app.locals.dbs.shoppingCartDb
  const dbCats = req.app.locals.dbs.catagoryDb
  Promise.all([
    db.resetAllProducts(),
    dbSC.clearAllCarts(),
    dbCats.saveAllCategories(categoriesPreload)
  ])
    .then(results => {
      if (results.every(result => result)) {
        return res.status(200).json({ success: true })
      }
      res.status(403).json({ success: false })
    })
    .catch(err => {
      res.status(500).json(`Internal server error: ${err}`)
    })
}

function convertToProduct(dbProduct: DbProduct): Product {
  const p: any = { ...dbProduct }
  p.id = p._id.toHexString()
  delete p._id
  return p
}

export function getAllProducts(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  db.getAllProducts$.pipe(take(1)).subscribe(
    dbProducts => {
      const products = dbProducts.map(convertToProduct)
      res.status(200).send(products)
    },
    err => {
      res.status(500).send(`Internal server error: ${err}`)
    }
  )
  // db.getAllProducts()
  //   .then(dbProducts => {
  //     const products = dbProducts.map(convertToProduct)
  //     res.status(200).send(products)
  //   })
  //   .catch(err => {
  //     res.status(500).send(`Internal server error: ${err}`)
  //   })
}

export function getProduct(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  const productId: string = req.params.id
  db.getProductById(productId)
    .then(dbProduct => {
      if (dbProduct) res.status(200).json(convertToProduct(dbProduct))
      else res.status(403).send(`Unable to find product with id: ${productId}`)
    })
    .catch(err => {
      res.status(500).send(`Internal server error: ${err}`)
    })
}

export function putProduct(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  const dbProduct: DbProduct = {
    title: req.body.title,
    price: req.body.price,
    imageUrl: req.body.imageUrl,
    category: req.body.category
  }
  const productId = req.params && req.params.id ? req.params.id : undefined
  if (!productId) {
    res.status(403).send(`Id missing in update product requestApp: ${dbProduct}`)
  } else {
    db.saveProduct(dbProduct, productId)
      .then(success => {
        if (success) {
          res.status(200).json({
            success
          })
        } else {
          res.status(403).send(`Unable to update product: ${dbProduct}`)
        }
      })
      .catch(err => {
        res.status(500).send(`Internal server error: ${err}`)
      })
  }
}

export function postProduct(req: RequestApp, res: ResponseApp) {
  const db: MongoProductDatabase = req.app.locals.dbs.productDb
  const dbProduct: DbProduct = {
    title: req.body.title,
    price: req.body.number,
    imageUrl: req.body.imageUrl,
    category: req.body.category
  }
  db.createProduct(dbProduct)
    .then(success => {
      if (success) {
        res.status(200).json({
          success
        })
      } else {
        res.status(403).send(`Unable to create product: ${dbProduct}`)
      }
    })
    .catch(err => {
      res.status(500).send(`Internal server error: ${err}`)
    })
}

export function getAllCategories(req: RequestApp, res: ResponseApp) {
  const db = req.app.locals.dbs.catagoryDb
  db.getAllCategories()
    .then(cats => {
      res.status(200).json(cats)
    })
    .catch(err => {
      res.status(500).send(`Internal server error loading categories: ${err}`)
    })
}
