# set heroku env
source server/keys/env-heroku.sh
# Ping config - not used?
# heroku config:set PING_INTERVAL_MSEC=$PING_INTERVAL_MSEC
# redis config
heroku config:set REDIS_DB_HOST=$REDIS_DB_HOST
heroku config:set REDIS_DB_PORT=$REDIS_DB_PORT
heroku config:set REDIS_DB_NUM=$REDIS_DB_NUM
heroku config:set REDIS_DB_AUTHCODE=$REDIS_DB_AUTHCODE
# mongo config
heroku config:set MONGO_DATABASE=$MONGO_DATABASE
heroku config:set MONGO_URL=$MONGO_URL
# must add heroku server to the ip whitelist of Atlas
# currently for heroku whitelist all addresses
heroku config:set RSA_PUBLIC_KEY="$(cat server/keys/public.key)"
heroku config:set RSA_PRIVATE_KEY="$(cat server/keys/private.key)"
heroku config:set HOST_URL=https://stormy-mountain-18015.herokuapp.com
heroku config:set DEFAULT_AVATAR_URL=https://stormy-mountain-18015.herokuapp.com/assets/default-gravatar.jpg
heroku config
