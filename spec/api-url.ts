import { serverConfig } from './test-config'
import fetch, { Response, RequestInit, Headers } from 'node-fetch'
import { Credentials, SignUpInfo } from '../server/database/user-db/user'
import { GenericMessage } from '../node_modules/bldg25-chat-server/lib/models/web-socket-messages'
import WebSocket from 'ws'
import { Subject, of, Observable } from 'rxjs'
import { switchMap, take, reduce, map } from 'rxjs/operators'

function jsonParseNull<T>(s: string): T | null {
  try {
    return JSON.parse(s) as T
  } catch (_error) {
    return null
  }
}

const serverType = 'http'
const serverUrl = `${serverType}://${serverConfig.serverHost}:${serverConfig.serverPort}`
const loginRequestUrl = `${serverUrl}/api/auth/login`
const signupRequestUrl = `${serverUrl}/api/auth/signup`
const logoutRequestUrl = `${serverUrl}/api/auth/logout`
const userRequestUrl = `${serverUrl}/api/auth/user`
const cleanRequestUrl = `${serverUrl}/api/auth/clean`

export const wsServerUrl = `ws://${serverConfig.serverHost}:${serverConfig.serverPort}`

const badXsrfToken = 'my-bad-xsrf-token'

interface ResponseJson<T> {
  bodyJson: T
  headers: Headers
  ok: boolean
  redirected: boolean
  status: number
  statusText: string
  // type: ResponseType;
  url: string
}

export function fromFetch<T>(url: string, init?: RequestInit): Observable<ResponseJson<T>> {
  return fromFetchResponse(url, init).pipe(
    map(r => {
      try {
        const bodyJson: T = JSON.parse(r.body.read().toString())
        const res: ResponseJson<T> = { ...r, bodyJson }
        return res
      } catch (_e) {
        throw _e
      }
    })
  )
}

export function fromFetchResponse(url: string, init?: RequestInit): Observable<Response> {
  return fromPromise(fetch(url, init))
  // return new ObservableFromPromise(fetch(url, init))
}

// export function fromFetchResponseOld(url: string, init?: RequestInit): Observable<Response> {
//   return new Observable(s => {
//     fetch(url, init)
//       .then(r => {
//         s.next(r)
//         s.complete()
//       })
//       .catch(e => s.error(e))
//   })
// }

export function fromPromise<T>(p: Promise<T>): Observable<T> {
  return new Observable(s => {
    p.then(r => {
      s.next(r)
      s.complete()
    }).catch(e => s.error(e))
  })
}

export class ObservableFromPromise<T> extends Observable<T> {
  constructor(p: Promise<T>) {
    super(s => {
      p.then(r => {
        s.next(r)
        s.complete()
      }).catch(e => s.error(e))
    })
  }
}

export interface TestUser {
  cred: Credentials
  cookie: string
  xsrfToken: string
  ws?: WebSocketSubject
  startLogin: number
  finishLogin: number
}

export class WebSocketSubject {
  static handshakeTimeout = 1000 * 1000
  webSocketMessages$ = new Subject<GenericMessage>() // Generic message
  constructor(wsUrl: string, cookie: string) {
    this.ws = new WebSocket(wsUrl, {
      headers: {
        cookie,
        'x-xsrf-token': badXsrfToken,
        'Content-Type': 'application/json'
      },
      handshakeTimeout: WebSocketSubject.handshakeTimeout
    }).on('message', (smRes: WebSocket.Data) => {
      const sm = jsonParseNull<GenericMessage>(smRes.toString())
      if (sm) this.webSocketMessages$.next(sm)
      else {
        this.webSocketMessages$.error(`bad json format in socket message`)
        this.close()
      }
    })
  }
  private ws: WebSocket
  close() {
    this.ws.close()
  }
}

function getWebSocketSubject(appId: string, cookie: string) {
  const webSocketServerUrl = `${wsServerUrl}/api-ws/?${appId}`
  return new WebSocketSubject(webSocketServerUrl, cookie)
}

function getCookies(res: Response): string {
  const cookies = res.headers.get('set-cookie') || ''
  let result = cookies.replace(/HttpOnly\,.+XSRF.TOKEN/, 'XSRF-TOKEN')
  result = result.replace(/Path=\/\, CHAT.TOKEN/, 'CHAT-TOKEN')
  return result
}

function getXsrfToken(cookies: string): string {
  const result = /XSRF\-TOKEN\=(.+);.+CHAT.TOKEN/.exec(cookies)
  return result ? result[1] : ''
}

export function getUserByEmail(email: string, adminUser: TestUser) {
  return fetch(`${userRequestUrl}/${email}`, {
    method: 'GET',
    headers: {
      cookie: adminUser.cookie,
      'x-xsrf-token': adminUser.xsrfToken,
      'Content-Type': 'application/json'
    }
  }).then(res => res.json())
}

function sendSignupUser$(info: SignUpInfo) {
  return fromFetchResponse(signupRequestUrl, {
    method: 'POST',
    body: JSON.stringify(info),
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

export function signupUser$(user: TestUser): Observable<boolean> {
  const info: SignUpInfo = {
    email: user.cred.email,
    passwordGroup: { password: user.cred.password, confirmPassword: user.cred.password },
    userName: user.cred.userName || '',
    avatarUrl: 'test-avatar-url'
  }
  user.startLogin = Date.now()
  return sendSignupUser$(info).pipe(
    map((res: Response) => _verifyLoginResponse(res, user)),
    switchMap(_verifyWebSocket$)
  )
}

function sendLoginUser$(tu: TestUser) {
  return fromFetchResponse(loginRequestUrl, {
    method: 'POST',
    body: JSON.stringify(tu.cred),
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

function _verifyLoginResponse(res: Response, user: TestUser) {
  if (res.status !== 200) {
    // console.error(`incorrect login response: ${user.cred.email}`)
    return null
  }
  user.cookie = getCookies(res)
  user.xsrfToken = getXsrfToken(user.cookie)
  user.finishLogin = Date.now()
  const appId = 'not-found-yet'
  const wsSubject = getWebSocketSubject(appId, user.cookie)
  user.ws = wsSubject
  return user.ws
}

export function loginUser$(user: TestUser) {
  user.startLogin = Date.now()
  return sendLoginUser$(user).pipe(
    map((res: Response) => _verifyLoginResponse(res, user)),
    switchMap(_verifyWebSocket$)
  )
}

function _verifyWebSocket$(wsSubject: WebSocketSubject | null) {
  if (!wsSubject) {
    return of(false)
  }
  return wsSubject.webSocketMessages$.pipe(
    take(3),
    reduce((acc, sm) => {
      const correct =
        sm.type === 'this-is-chat-you' ||
        sm.type === 'all-users' ||
        sm.type === 'room-list' ||
        sm.type === 'active-user-ids'
      if (!correct) {
        console.error(`other socket message: ${sm.type}`)
      }
      return acc && correct
    }, true)
  )
}

export function logoutUser$(user: TestUser) {
  if (user.ws) {
    user.ws.close()
  } else {
    console.error(`Attempt to logout without valid websocket: ${user.cred.email}`)
  }
  return fromFetchResponse(logoutRequestUrl, {
    method: 'POST',
    headers: {
      cookie: user.cookie,
      'x-xsrf-token': user.xsrfToken,
      'Content-Type': 'application/json'
    }
  }).pipe(
    map(r => {
      if (r.status !== 200) {
        return false
      }
      if (user.ws) user.ws.close()
      return true
    })
  )
}

function sendDeleteUser$(admin: TestUser, userEmail: string) {
  return fromFetchResponse(`${userRequestUrl}/${userEmail}`, {
    method: 'DELETE',
    headers: {
      cookie: admin.cookie,
      'x-xsrf-token': admin.xsrfToken,
      'Content-Type': 'application/json'
    }
  })
}

export function deleteUser$(userEmail: string, admin: TestUser) {
  return sendDeleteUser$(admin, userEmail).pipe(map((res: Response) => res.status === 200))
}

export function clean$(admin: TestUser) {
  return fromFetchResponse(cleanRequestUrl, {
    method: 'POST',
    headers: {
      cookie: admin.cookie,
      'x-xsrf-token': admin.xsrfToken,
      'Content-Type': 'application/json'
    }
  }).pipe(
    map(r => {
      if (r.status !== 200) {
        console.error(`Unable to clean db: ${r.status}`)
        return false
      } else {
        return true
      }
    })
  )
}
