import { testConfig } from './test-config'
import { TestUser, logoutUser$, loginUser$, signupUser$, deleteUser$, clean$ } from './api-url'

const adminUser: TestUser = {
  cred: testConfig.weirdCredentials,
  cookie: '',
  xsrfToken: '',
  startLogin: 0,
  finishLogin: 0
}
const testUser: TestUser = {
  cred: {
    email: testConfig.signupCredentials.email,
    password: testConfig.signupCredentials.password
  },
  cookie: '',
  xsrfToken: '',
  startLogin: 0,
  finishLogin: 0
}

describe('WS tests', () => {
  it(`should accept ${adminUser.cred.email} signup and test initial websocket handshake`, (done: DoneFn) => {
    signupUser$(adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should accept ${testUser.cred.email} signup and test initial websocket handshake`, (done: DoneFn) => {
    signupUser$(testUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should delete ${testUser.cred.email} with admin credentials.`, (done: DoneFn) => {
    deleteUser$(testUser.cred.email, adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should accept ${testConfig.weirdCredentials.email} logout with good credentials`, (done: DoneFn) => {
    logoutUser$(adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should accept ${testConfig.weirdCredentials.email} login and test initial websocket handshake`, (done: DoneFn) => {
    loginUser$(adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should delete ${testConfig.weirdCredentials.email} with admin credentials.`, (done: DoneFn) => {
    deleteUser$(testConfig.weirdCredentials.email, adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
  it(`should clean the chat database with admin credentials.`, (done: DoneFn) => {
    clean$(adminUser).subscribe(result => {
      expect(result).toBeTruthy()
      done()
    })
  })
})
