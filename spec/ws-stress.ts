import { testUsers } from './test-config'
import { SignUpInfo } from '../server/database/user-db/user'
import {
  TestUser,
  signupUser$,
  // signupUserOld$,
  loginUser$,
  logoutUser$,
  deleteUser$,
  WebSocketSubject,
  clean$
} from './api-url'

import { range, from, concat, defer } from 'rxjs'
import { map, tap, mergeMap, finalize } from 'rxjs/operators'

const newUsers: TestUser[] = []
const admin: TestUser = testUsers.weirdUser

function createUserSignUp(i: number): SignUpInfo {
  return {
    email: `testuser${i}@user.com`,
    passwordGroup: { password: `Password10`, confirmPassword: `Password10` },
    userName: `Test User ${i}`,
    avatarUrl: 'test-avatar-url'
  }
}

const verbose = true
function vlog(s: string) {
  if (verbose) console.log(s)
}
const numUsers = 80
WebSocketSubject.handshakeTimeout = 2000 * 1000

range(numUsers)
  .pipe(
    map(createUserSignUp),
    map(info => {
      const tu: TestUser = {
        cred: { email: info.email, password: info.passwordGroup.password },
        cookie: '',
        xsrfToken: '',
        startLogin: 0,
        finishLogin: 0
      }
      newUsers.push(tu)
      return tu
    })
  )
  .subscribe(
    () => {},
    err => console.error(err),
    () => vlog(`created test users array with length: ${newUsers.length}`)
  )

concat(
  signupUser$(admin).pipe(tap(_n => vlog(`signup admin result: ${_n}`))),
  // logoutUser$(admin).pipe(tap(_n => vlog(`logout admin after signup result: ${_n}`))),
  defer(() => logoutUser$(admin).pipe(tap(_n => vlog(`logout admin after signup result: ${_n}`)))),
  signupUsers$(newUsers),
  logoutUsers$(newUsers),
  defer(() =>
    loginUser$(admin).pipe(
      tap(_n => {
        vlog(`login admin result: ${_n}`)
      })
    )
  ),
  defer(() => deleteUsers$(admin)),
  defer(() =>
    deleteUser$(admin.cred.email, admin).pipe(tap(_n => vlog(`delete admin result: ${_n}`)))
  ),
  defer(() => clean$(admin).pipe(tap(_n => vlog(`Clean result: ${_n}`)))),
  defer(() => logoutUser$(admin).pipe(tap(_n => vlog(`logout admin result: ${_n}`))))
).subscribe(
  _x => {},
  // _x => vlog(`concat observable: ${_x}`),
  _x => console.error(`concat observable error: ${_x}`),
  () => {
    const aveargeResponse =
      newUsers.reduce((p, u) => p + u.finishLogin - u.startLogin, 0) / numUsers
    const maxResponse = newUsers.reduce((p, u) => {
      const c = u.finishLogin - u.startLogin
      return p > c ? p : c
    }, 0)
    console.log(`Max response: ${(maxResponse / 1000).toFixed(3)} secs`)
    console.log(`Average response: ${(aveargeResponse / 1000).toFixed(3)} secs`)
    console.log(`concat observable complete.`)
  }
)

function signupUsers$(tUsers: TestUser[]) {
  let signups = 0
  return from<TestUser[]>(tUsers).pipe(
    mergeMap(signupUser$),
    tap(_n => {
      if (_n) signups++
    }),
    finalize(() => vlog(`signups complete: ${signups}`))
  )
}

function deleteUsers$(adminUser: TestUser) {
  let deletes = 0
  return range(numUsers).pipe(
    map(createUserSignUp),
    map(userSignup => userSignup.email),
    mergeMap(email => deleteUser$(email, adminUser)),
    tap(_n => {
      if (_n) deletes++
    }),
    finalize(() => vlog(`user deletes complete: ${deletes}`))
  )
}

function logoutUsers$(tUsers: TestUser[]) {
  let logouts = 0
  return from<TestUser[]>(tUsers).pipe(
    mergeMap(logoutUser$),
    tap(_n => {
      if (_n) logouts++
    }),
    finalize(() => vlog(`logouts complete: ${logouts}`))
  )
}
