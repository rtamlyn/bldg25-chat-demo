import { testUsers, poorPasswordUser, blankPasswordUser } from './test-config'
import { signupUser$, logoutUser$, loginUser$, deleteUser$, getUserByEmail } from './api-url'

describe('Api Auth endpoints with node-fetch', () => {
  it(`should accept ${testUsers.weirdUser.cred.email} signup with weird credentials`, (done: DoneFn) => {
    signupUser$(testUsers.weirdUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })
  it(`should accept ${testUsers.weirdUser.cred.email} logout`, (done: DoneFn) => {
    logoutUser$(testUsers.weirdUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })
  it(`should block second signup of ${testUsers.weirdUser.cred.email} with weird credentials`, (done: DoneFn) => {
    signupUser$(testUsers.weirdUser).subscribe(success => {
      expect(success).toBeFalsy()
      done()
    })
  })
  it(`should block signup of poor password ${poorPasswordUser.cred.email} with poor password`, (done: DoneFn) => {
    signupUser$(poorPasswordUser).subscribe(success => {
      expect(success).toBeFalsy()
      done()
    })
  })
  it(`should block signup of blank password ${blankPasswordUser.cred.email} with blank password`, (done: DoneFn) => {
    signupUser$(blankPasswordUser).subscribe(success => {
      expect(success).toBeFalsy()
      done()
    })
  })
  it(`should accept ${testUsers.weirdUser.cred.email} login`, (done: DoneFn) => {
    loginUser$(testUsers.weirdUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })
  it(`should block ${testUsers.badUser.cred.email} login`, (done: DoneFn) => {
    loginUser$(testUsers.badUser).subscribe(success => {
      expect(success).toBeFalsy()
      done()
    })
  })

  it(`should accept ${testUsers.newUser.cred.email} signup with new credentials`, (done: DoneFn) => {
    signupUser$(testUsers.newUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })

  it(`should get new user info ${testUsers.newUser.cred.email} using admin authorization`, (done: DoneFn) => {
    getUserByEmail(testUsers.newUser.cred.email, testUsers.weirdUser).then(user => {
      const timeSinceCreation = Date.now() - user.created
      expect(timeSinceCreation).toBeGreaterThan(0)
      done()
    })
  })

  it(`should fail to delete user ${testUsers.newUser.cred.email} using bad authorization`, (done: DoneFn) => {
    deleteUser$(testUsers.newUser.cred.email, testUsers.newUser).subscribe(success => {
      expect(success).toBeFalsy()
      done()
    })
  })

  it(`should delete user ${testUsers.newUser.cred.email} using good authorization`, (done: DoneFn) => {
    deleteUser$(testUsers.newUser.cred.email, testUsers.weirdUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })

  it(`should delete weird user ${testUsers.weirdUser.cred.email} using good authorization`, (done: DoneFn) => {
    deleteUser$(testUsers.weirdUser.cred.email, testUsers.weirdUser).subscribe(success => {
      expect(success).toBeTruthy()
      done()
    })
  })
})
