import { TestUser } from './api-url'

const emptyUser: TestUser = {
  cred: {
    email: 'fsYB4k9qHaB95NpN@gmail.com',
    password: 'KpSUk45-U7Q5x*+6'
  },
  cookie: '',
  xsrfToken: '',
  startLogin: 0,
  finishLogin: 0
}

export const poorPasswordUser: TestUser = {
  cred: {
    email: 'poorPassword@gmail.com',
    password: '111'
  },
  cookie: '',
  xsrfToken: '',
  startLogin: 0,
  finishLogin: 0
}

export const blankPasswordUser: TestUser = {
  cred: {
    email: 'blankPassword@gmail.com',
    password: ''
  },
  cookie: '',
  xsrfToken: '',
  startLogin: 0,
  finishLogin: 0
}

export const testUsers: { [user: string]: TestUser } = {
  weirdUser: {
    ...emptyUser,
    cred: {
      email: 'fsYB4k9qHaB95NpN@gmail.com',
      password: 'KpSUk45-U7Q5x*+6'
    }
  },
  studentUser: {
    ...emptyUser,
    cred: {
      email: 'student@gmail.com',
      password: 'Password10'
    }
  },
  newUser: {
    ...emptyUser,
    cred: {
      email: 'new@gmail.com',
      password: 'Password20'
    }
  },
  badUser: {
    ...emptyUser,
    cred: {
      email: 'admin@gmail.com',
      password: 'Password10x'
    }
  }
}

export const testConfig = {
  weirdCredentials: {
    email: 'fsYB4k9qHaB95NpN@gmail.com',
    password: 'KpSUk45-U7Q5x*+6'
  },
  studentCredentials: {
    email: 'student@gmail.com',
    password: 'Password10'
  },
  signupCredentials: {
    email: 'new@gmail.com',
    password: 'Password20'
  },
  badCredentials: {
    email: 'admin@gmail.com',
    password: 'Password10x'
  }
}

export const serverConfig = {
  serverHost: 'localhost',
  serverPort: 9000
}
