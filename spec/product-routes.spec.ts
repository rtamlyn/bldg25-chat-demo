import { serverConfig } from './test-config'
import { fromFetch } from './api-url'
import { Product, Category } from '../server/product/product-model'

const serverType = 'http'
const serverUrl = `${serverType}://${serverConfig.serverHost}:${serverConfig.serverPort}`
const productRequestUrl = `${serverUrl}/api/product`

let fetchedProducts: Product[] = []

describe('Api product endpoints', () => {
  it(`should fetch categories list`, (done: DoneFn) => {
    fromFetch<Category[]>(`${productRequestUrl}/categories`).subscribe(res => {
      const cats = res.bodyJson
      const l = Object.keys(cats).length
      expect(l).toBe(4)
      done()
    })
  })

  it(`should fetch product list`, (done: DoneFn) => {
    fromFetch<Product[]>(`${productRequestUrl}/all`).subscribe(res => {
      fetchedProducts = res.bodyJson
      expect(fetchedProducts.length).toBe(21)
      done()
    })
  })

  it(`should fetch the last product in the list`, (done: DoneFn) => {
    const productToFetch = fetchedProducts[fetchedProducts.length - 1]
    const id = productToFetch.id
    if (id) {
      fromFetch<Product>(`${productRequestUrl}/${id}`).subscribe(res => {
        const title = res.bodyJson.title
        expect(title).toBe(productToFetch.title)
        done()
      })
    } else {
      expect(id).toBeTruthy()
      done()
    }
  })
})
