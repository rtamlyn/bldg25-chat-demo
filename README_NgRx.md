# NgRx Setup

- Three types of actions

  - Command - use verb; single handler may provide reply
  - Document - use noun; document was updated; do not expect reply may have multiple handlers
  - Event - say what happened do not expect reply may have multiple handlers
    effect - use to send data to backend and then call action to update store

  side effects are the hard part; reducers are easy simple pure functions

  - effects classes
    - action deciders
      - Filter
      - content-based decider
      - context based decider
      - splitter
      - aggregator
    - action transformers
      - content enricher
      - normalizer and canonical actions
    - now send action to reducer(s)

- Pitfalls

  - Reusing actions - instead use action event
  - Generic Action Types

  Do this instead:
  [Menu Page] Add Taco
  [Taco Details Page] Add Taco
  then pipe the above actions in an effect into the taco api service addOne which sends to the reducer?
  [source] Event

  - Action Subtypes
  - Action Filters

  Above will cause lots of action types but that is best

  Exceptions are still there with above guidelines

  Focus on clarity over brevity
  Empathetic in your architecture design choices

  Write Actions first

  - User Actions
    - [Login Page] Login
    - [Menu Page] Add Burger
    - [Cart] Add Hot Dog
    - [Cart] Checkout
  - Backend Actions
    - [Auth API] Login Success
    - [Cart API] Checkout Success
  - Device Actions
    - [WebSocket] Open
    - [WebSocket] Disconnected
    - [IndexDB] Save Success

- Selectors

  Fundamental Concept: Query to the store

  - Provide a query API for views
  - Reduce Action Boilerplate
  - Simplify reducers
  - Memoization
  - Routing State

## NgRx Setup Commands

```bash
npm install @ngrx/schematics --D
npm install @ngrx/{store,effects,entity,store-devtools} --save

# generate main ngrx store
ng g @ngrx/schematics:store State --root --statePath state --module app.module.ts -d
ng g @ngrx/schematics:feature state/Auth --flat false --reducers index.ts -d
ng g @ngrx/schematics:action LoginPage --flat false -d

ng g service state/state -d # ???
ng g @ngrx/schematics:entity state/Product --flat false --reducers index.ts -d
# in progress

ng g @ngrx/schematics:feature state/Admin --flat false --reducers index.ts -d

```
