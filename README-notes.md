# Chat 6

This demo shows how to add bldg25-chat to an existing angular 6+ application

See Setup Demo App below

[![Build Status][travis-image]][travis-url] [![Coverage Status][coveralls-image]][coveralls-url]
[![Known Vulnerabilities](https://snyk.io/test/github/nickmerwin/node-coveralls/badge.svg)](https://snyk.io/test/github/nickmerwin/node-coveralls)

[travis-image]: https://travis-ci.org/nickmerwin/node-coveralls.svg?branch=master
[travis-url]: https://travis-ci.org/nickmerwin/node-coveralls
[coveralls-image]: https://coveralls.io/repos/nickmerwin/node-coveralls/badge.svg?branch=master&service=github
[coveralls-url]: https://coveralls.io/github/nickmerwin/node-coveralls?branch=master

## Develop with Mongo

- pre load users data

```bash
# run reset user debug launch task
npm run pre-load-app-data
```

- Local development with debug server with atlas mongo

```bash
# if needed: redis-server ./server/database/redis.conf # launch local redis server
# start debug server from vs-code
npm run start
# npm run start-secure # to run https server locally
```

- Mongo Shell commands if needed

```bash
MONGO_URL='mongodb+srv://chat-dev-server:W_%3BjB%5Bw%7B%7DD2%2BRkQK@dev-vejwg.mongodb.net/test?retryWrites=true'
mongo $MONGO_URL
show dbs
use demo
show collections
db['chat-users'].getIndexes()
db['chat-users'].dropIndex('appId_-1')
```

- Run tests

```bash
# start api server in debug
# redis-server ./server/database/redis.conf  # ONLY if needed launch local redis server

npm run api-test # back end tests
npm run api-stress # stress test of backend
ng test # front end tests
ng test --code-coverage # then open ./coverage/index.html in browser
npm run test-code-coverage

# Use dev branch for code dev. When merge and push master to github trigger ci
ng test --code-coverage && cat ./coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js && rm -rf ./coverage
```

- Local run of prod

```bash
npm outdated
npm update # to get latest published version of chat modules
npm run build # to build demo app angular code and server code
node dist/server.js --prod # test with Mongo only
node dist/server.js --prod --useRedisCategories  # OR test with redis categories
node dist/server.js --prod --useRedisUserDb --useRedisChatDb --useRedisCategories  # OR test with redis categories and users
# go to localhost:9000
```

- Test https version locally

```bash
# be sure to build first
node dist/server.js --secure # start server in secure mode
npm run start-secure # start ng server in secure mode
```

- Deploy to digital ocean

  - instructions [here](https://www.youtube.com/watch?v=oykl1Ih9pMg) with notes [here](https://gist.github.com/bradtraversy/cd90d1ed3c462fe3bddd11bf8953a896)

  - setup digital ocean account

  ```bash

  ```

## Develop with Redis Version

- redis install:

```bash
# download redis tar ball from [Redis.io](https://redis.io/) and unzip
make
make test
cd ~/redis-4.0.6/src/  # location of redis-server and redis-cli
# Add above dir to your path so scripts can find redis
```

### Initialize redis database(s)

- Local Redis

```bash
redis-server ./server/database/redis.conf
redis-cli -p 6379 -a this_should_be_a_secret_authcode
npm run pre-load-user-data
```

- Redis Shell command if needed for cloud redis hosts

```bash
source .env # for dev redis server
# source server/keys/env-heroku.sh # for heroku redis server
redis-cli -h $REDIS_DB_HOST -p $REDIS_DB_PORT -a $REDIS_DB_AUTHCODE -n $REDIS_DB_NUM

# heroku redis database cli
REDIS_DB_HOST='redis-12907.c81.us-east-1-2.ec2.cloud.redislabs.com'
REDIS_DB_PORT=12907
REDIS_DB_AUTHCODE="rrisAmzR1wQW63HTtEttK8hdHm526M68"
REDIS_DB_NUM="0"
redis-cli -h $REDIS_DB_HOST -p $REDIS_DB_PORT -a $REDIS_DB_AUTHCODE -n $REDIS_DB_NUM
```

### Redis Launch Scenarios - These need updating

- Local development with debug server with local redis server

```bash
redis-server ./server/database/redis.conf # launch local redis server
# Launch server with vs-code debugger
npm start
```

- Local development with local redis server

```bash
redis-server ./server/database/redis.conf  # launch local redis server
# run server build in vs code with ctrl-shft-b
npm run start-server
npm start
```

- Local Devlopment with RedisToGo Server

```bash
# cntl-shft-b to build server code in vs-code
node dist/server.js --dbHost $REDIS_DB_HOST --dbPort $REDIS_DB_PORT --dbAuth $REDIS_DB_AUTHCODE -n $REDIS_DB_NUM
npm start
```

- Local test of prod with Redislabs Server (used by heroku server)

```bash
npm outdated
npm update # to get latest published version of chat modules
npm run build # to build demo app angular code and server code
# cntl-shft-b to build server in vs-code
# npm run build-server to build only server code
#
# If errors in build occur, try deleting npm_modules and npm i
#
node dist/server.js --prod  --useRedisChatDb --useRedisUserDb --useRedisCategories
# go to localhost:9000
```

- remove all chat keys from database

```bash
redis-cli -h $REDIS_DB_HOST -p $REDIS_DB_PORT -a $REDIS_DB_AUTHCODE -n $REDIS_DB_NUM KEYS chat* | xargs redis-cli -h $REDIS_DB_HOST -p $REDIS_DB_PORT -a $REDIS_DB_AUTHCODE -n $REDIS_DB_NUM DEL
```

## Miscellaneous

- Fixed random test failure by adding afterEach()... in products.component.spec.ts and others. This appears from time to time.

- if needed, clear debug process using port 9000 that failed to terminate

  ```bash
  lsof -i tcp:9000
  kill <pid of process using port 9000>
  ```

- Angular generate commands

  ```bash
  # to create a directive
  ng g directive shared/directives/rbacAllow -d
  ng g @angular/material:material-dashboard --name core/main-dashboard -d

  # genarate state for a feature module
  ng generate feature starships/Ships -m starships/starships.module.ts --group --spec false

  ```

- google maps package

```bash
npm i --save @agm/core
```
